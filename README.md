# WorldSynth Patcher
The WorldSynth Patcher repository has been merged with the WorldSynth Engine repository, and development in this repository is discontinued.  
Please see the new combined repository at: https://gitlab.com/wsynth/worldsynth