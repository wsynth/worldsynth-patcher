/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmapOverlay;
import net.worldsynth.glpreview.fx.Overlay3dViewer;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class HeightmapOverlay3DRender extends AbstractPreviewRender {
	
	private final Overlay3dViewer overlayViewer;
	
	public HeightmapOverlay3DRender() {
		super();
		overlayViewer = new Overlay3dViewer();
		overlayViewer.setPrefSize(450, 450);
		getChildren().add(overlayViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        overlayViewer.setLayoutX(x);
        overlayViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeHeightmapOverlay castData = (DatatypeHeightmapOverlay) data;
		overlayViewer.setHeightmap(castData.heightMap, castData.colorMap, 255, Math.max(castData.width, castData.length));
	}
}
