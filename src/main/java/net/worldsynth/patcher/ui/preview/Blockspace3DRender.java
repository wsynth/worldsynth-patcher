/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.glpreview.fx.Blockspace3dViewer;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Blockspace3DRender extends AbstractPreviewRender {
	
	private final Blockspace3dViewer blockspaceViewer;
	
	public Blockspace3DRender() {
		super();
		blockspaceViewer = new Blockspace3dViewer();
		blockspaceViewer.setPrefSize(450, 450);
		getChildren().add(blockspaceViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        blockspaceViewer.setLayoutX(x);
        blockspaceViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeBlockspace castData = (DatatypeBlockspace) data;
		blockspaceViewer.setBlockspace(castData.getBlockspace());
	}
}
