/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class Valuespace3DRender extends AbstractPreviewRenderCanvas {
	
	@SuppressWarnings("unused")
	private float[][][] valuespace;
	
	public Valuespace3DRender() {
		super();
	}
	
	public void setVoxelValuespace(float[][][] valuespace) {
		this.valuespace = valuespace;
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeValuespace castData = (DatatypeValuespace) data;
		setVoxelValuespace(castData.valuespace);
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.RED);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}
