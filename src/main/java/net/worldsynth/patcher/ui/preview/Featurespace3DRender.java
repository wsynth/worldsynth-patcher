/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.glpreview.fx.Featurespace3dViewer;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Featurespace3DRender extends AbstractPreviewRender {
	
	private final Featurespace3dViewer featurespaceViewer;
	
	public Featurespace3DRender() {
		super();
		featurespaceViewer = new Featurespace3dViewer();
		featurespaceViewer.setPrefSize(450, 450);
		getChildren().add(featurespaceViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        featurespaceViewer.setLayoutX(x);
        featurespaceViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeFeaturespace castData = (DatatypeFeaturespace) data;
		featurespaceViewer.setFeaturespace(castData.points, castData.x, castData.y, castData.z, castData.width, castData.height, castData.length);
	}
}
