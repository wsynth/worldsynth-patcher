/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class HeightmapGrayscaleRender extends AbstractPreviewRenderCanvas {
	
	private float[][] heightmap;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		this.heightmap = castData.getHeightmap();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(heightmap != null) {
			
			double xOffset = (getWidth() - heightmap.length)/2;
			double yOffset = (getHeight() - heightmap[0].length)/2;
			
			for(int x = 0; x < heightmap.length; x++) {
				for(int y = 0; y < heightmap[x].length; y++) {
					g.setFill(heightToColor(heightmap[x][y]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
		}
	}
	
	private Color heightToColor(float height) {
		return Color.gray(height);
	}
}
