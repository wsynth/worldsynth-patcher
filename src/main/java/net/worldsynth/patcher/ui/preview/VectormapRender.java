/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

public class VectormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] vectorfield;
	
	private float gain = 10.0f;
	
	public VectormapRender() {
		setOnScroll(e -> {
			
			float maxGain = 100.0f;
			float minGain = 0.05f;
			
			double lastGain = gain;
			gain += e.getDeltaY()/e.getMultiplierY();
			if(gain < minGain) gain = minGain;
			else if(gain > maxGain) gain = maxGain;
			
			if(lastGain != gain) {
				paint();
			}
		});
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeVectormap castData = (DatatypeVectormap) data;
		this.vectorfield = castData.vectorField;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(vectorfield != null) {
			
			double xOffset = (getWidth() - vectorfield.length)/2;
			double yOffset = (getHeight() - vectorfield[0].length)/2;
			
			for(int x = 0; x < vectorfield.length; x+=10) {
				for(int y = 0; y < vectorfield[x].length; y+=10) {
					g.setStroke(Color.LIGHTGRAY);
					g.strokeLine(x+xOffset, y+yOffset, (double)x+xOffset+vectorfield[x][y][0]*gain, (double)y+yOffset+vectorfield[x][y][1]*gain);
					g.setFill(Color.YELLOW);
					g.fillRect(x+xOffset, y+xOffset, 1, 1);
				}
			}
		}
	}
}
