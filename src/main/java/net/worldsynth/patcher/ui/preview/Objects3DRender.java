/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.glpreview.fx.Object3dViewer;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Objects3DRender extends AbstractPreviewRender {
	
	private final Object3dViewer objectViewer;
	
	public Objects3DRender() {
		super();
		objectViewer = new Object3dViewer();
		objectViewer.setPrefSize(450, 450);
		getChildren().add(objectViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        objectViewer.setLayoutX(x);
        objectViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeObjects castData = (DatatypeObjects) data;
		if(castData.objects.length > 0) {
			objectViewer.setObject(castData.objects[0].getObject());
		}
		else {
			objectViewer.setObject(new CustomObject(new Block[0]));
		}
	}
}
