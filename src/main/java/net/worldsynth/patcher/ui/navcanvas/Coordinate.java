/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.navcanvas;

public class Coordinate {
	
	public double x;
	public double y;
	
	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinate(Pixel p, NavigationalCanvas navCanvas) {
		double x = p.x;
		double y = p.y;
		
		x -= navCanvas.getWidth() / 2.0;
		y -= navCanvas.getHeight() / 2.0;
		
		x += navCanvas.getCenterCoordinateX() * navCanvas.getZoom();
		y += navCanvas.getCenterCoordinateY() * navCanvas.getZoom();
		
		x /= navCanvas.getZoom();
		y /= navCanvas.getZoom();
		
		this.x = x;
		this.y = y;
	}
}