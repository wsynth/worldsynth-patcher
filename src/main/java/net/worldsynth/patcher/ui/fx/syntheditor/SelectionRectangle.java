/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.util.ArrayList;

import net.worldsynth.modulewrapper.ModuleWrapper;

public class SelectionRectangle {
	private double startCoordinateX;
	private double startCoordinateY;
	private double endCoordinateX;
	private double endCoordinateY;
	
	public SelectionRectangle(double startCoordinateX, double startCoordinateY) {
		this.endCoordinateX = this.startCoordinateX = startCoordinateX;
		this.endCoordinateY = this.startCoordinateY = startCoordinateY;
	}
	
	public void setEndCoordinate(double endCoordinateX, double endCoordinateY) {
		this.endCoordinateX = endCoordinateX;
		this.endCoordinateY = endCoordinateY;
	}
	
	public double getX1() {
		if(startCoordinateX < endCoordinateX) {
			return startCoordinateX;
		}
		return endCoordinateX;
	}
	
	public double getY1() {
		if(startCoordinateY < endCoordinateY) {
			return startCoordinateY;
		}
		return endCoordinateY;
	}
	
	public double getX2() {
		if(startCoordinateX > endCoordinateX) {
			return startCoordinateX;
		}
		return endCoordinateX;
	}
	
	public double getY2() {
		if(startCoordinateY > endCoordinateY) {
			return startCoordinateY;
		}
		return endCoordinateY;
	}
	
	public ModuleWrapper[] getDevicesInside(SynthEditorPane synthEditor) {
		ArrayList<ModuleWrapper> containedDeviceList = new ArrayList<ModuleWrapper>();
		for(ModuleWrapper d: synthEditor.getSynth().getWrapperList()) {
			if(contains(d.posX, d.posY) && contains(d.posX+d.wrapperWidth, d.posY+d.wrapperHeight)) {
				containedDeviceList.add(d);
			}
		}
		ModuleWrapper[] list = new ModuleWrapper[0];
		return containedDeviceList.toArray(list);
	}
	
	private boolean contains(double x, double y) {
		if(x < getX1() || x > getX2()) {
			return false;
		}
		if(y < getY1() || y > getY2()) {
			return false;
		}
		return true;
	}
}
