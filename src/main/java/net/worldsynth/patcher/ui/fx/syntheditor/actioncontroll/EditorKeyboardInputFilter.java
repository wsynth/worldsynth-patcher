/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll;

import javafx.event.EventType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class EditorKeyboardInputFilter {
	public EventType<KeyEvent> keyEventType = null;
	public KeyCode keyCode = null;
	
	public InputState modifierCtrl;
	public InputState modifierShift;
	public InputState modifierAlt;
	
	public InputState mouseOverModule;
	public InputState mouseOverIo;
	public InputState tempModule;
	public InputState tempConnector;
	public InputState tempSynth;
	
	public InputState areaSelector;
	
	public EditorKeyboardInputFilter(EventType<KeyEvent> keyEventType, KeyCode keyCode, InputState modifierCtrl, InputState modifierShift, InputState modifierAlt, InputState mouseOverModule, InputState mouseOverIo, InputState tempModule, InputState tempConnector, InputState tempSynth, InputState areaSelector) {
		this.keyEventType = keyEventType;
		this.keyCode = keyCode;
		
		this.modifierCtrl = modifierCtrl;
		this.modifierShift = modifierShift;
		this.modifierAlt = modifierAlt;
		
		this.mouseOverModule = mouseOverModule;
		this.mouseOverIo = mouseOverIo;
		this.tempModule = tempModule;
		this.tempConnector = tempConnector;
		this.tempSynth = tempSynth;
		
		this.areaSelector = areaSelector;
	}
}
