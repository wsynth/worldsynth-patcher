/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.io.File;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.module.ModuleMacro;
import net.worldsynth.modulewrapper.ModuleConnector;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.modulewrapper.ModuleWrapperIO;
import net.worldsynth.patcher.ui.fx.WorldSynthEditorController;
import net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll.EditorKeyboardInputFilter;
import net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll.EditorMouseInputFilter;
import net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll.InputState;
import net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll.KeyboardAction;
import net.worldsynth.patcher.ui.fx.syntheditor.actioncontroll.MouseAction;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.patcher.ui.navcanvas.Pixel;
import net.worldsynth.patcher.ui.syntheditor.history.AbstractEditorHistoryEvent;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistoryEventModuleAddRemove;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistoryEventModuleEdit;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistoryEventModuleMove;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistoryRegister;
import net.worldsynth.standalone.ui.stage.DeviceNameEditorStage;
import net.worldsynth.standalone.ui.stage.ModuleParametersStage;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;

public class SynthEditorPane extends Pane implements NavigationalCanvas {
	
	private Tab parentTab;
	private Canvas canvas;
	private TreeItem<ModuleWrapper> deviceTree;
	private DeviceSearchPopupFx deviceSearchPopup = new DeviceSearchPopupFx(this);
	
	private Synth synth;
	private EditorHistoryRegister history = new EditorHistoryRegister(20);
	private boolean unsavedChanges = false;
	
	private File synthFile = null;
	
	/**
	 * The {@link ModuleWrapper} that the mouse is hovering over
	 */
	private ModuleWrapper wrapperOver = null; 
	/**
	 * The {@link DeviceIO} that the mouse is hovering over
	 */
	private ModuleWrapperIO wrapperIoOver = null;
	/**
	 * The list of {@link ModuleWrapper}s that are currently selected
	 */
	private ArrayList<ModuleWrapper> selectedWrappers = new ArrayList<ModuleWrapper>();
	
	private TempSynth tempSynth = null;
	private ModuleWrapper tempWrapper = null;
	private ModuleConnector tempConnector = null;
	private SelectionRectangle selectionRectangle = null;
	private SelectionLasso selectionLasso = null;
	
	private double centerCoordX = 0.0;
	private double centerCoordY = 0.0;
	private double zoom = 1.0;
	
	private MouseListener mouseListener = new MouseListener();
	private KeyboardListener keyboardListener = new KeyboardListener();
	
 	public SynthEditorPane(Synth synth, Tab parentTab) {
		this(synth, null, parentTab);
	}
	
	public SynthEditorPane(Synth synth, File synthFile, Tab parentTab) {
		//Setup style
		getStyleClass().add("syntheditor");
		
		//Construct
		this.synth = synth;
		this.synthFile = synthFile;
		this.parentTab = parentTab;
		
		//Register event handlers for synth content
		for(ModuleWrapper d: synth.getWrapperList()) {
			//Register parameters change handler
			//TODO When implementing caching to a forwards invalidation of other wrappers
			d.module.setOnModuleParametersChange(e -> {
				WorldSynthEditorController.instance.updatePreview();
				history.addHistoryEvent(new EditorHistoryEventModuleEdit(d, e.getOldParametersElement(), e.getNewParametersElement()));
				registerUnsavedChangePerformed();
			});
			
			//Register bypass change handler
			//TODO When implementing caching to a forwards invalidation of other wrappers
			d.module.setOnModuleBypassChange(e -> {
				repaint();
				WorldSynthEditorController.instance.updatePreview();
			});
		}
		
		//Build tree browser for the synth
		deviceTree = buldWrapperTree(synth);
		
		//Build editor canvas
		canvas = new Canvas(1100, 800);
		getChildren().add(canvas);
		
		canvas.addEventHandler(MouseEvent.ANY, mouseListener);
		canvas.addEventHandler(KeyEvent.ANY, keyboardListener);
		
		canvas.addEventHandler(ScrollEvent.SCROLL, e -> {
			//Do not zoom on direct input
			if(e.isDirect()) return;
			
			double maxZoom = 5.0;
			double minZoom = 0.05;
			
			zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
			if(zoom < minZoom) zoom = minZoom;
			else if(zoom > maxZoom) zoom = maxZoom;
			repaint();
		});
		
		repaint();
	}
	
	private TreeItem<ModuleWrapper> buldWrapperTree(Synth synth) {
		TreeItem<ModuleWrapper> treeRoot = new TreeItem<ModuleWrapper>();
		
		for(ModuleWrapper d: synth.getWrapperList()) {
			TreeItem<ModuleWrapper> deviceItem = new TreeItem<ModuleWrapper>(d);
			treeRoot.getChildren().add(deviceItem);
		}
		
		return treeRoot;
	}
	
	//---------------------------------------------------------------------------//
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
        repaint();
    }
	
	//---------------------------------------------------------------------------//
	
	@Override
	public double getZoom() {
		return zoom;
	}
	
	@Override
	public double getCenterCoordinateX() {
		return centerCoordX;
	}
	
	@Override
	public double getCenterCoordinateY() {
		return centerCoordY;
	}
	
	public Synth getSynth() {
		return synth;
	}
	
	public TreeItem<ModuleWrapper> getDeviceTree() {
		return deviceTree;
	}
	
	public File getSaveFile() {
		return synthFile;
	}
	
	public void setSaveFile(File file) {
		synthFile = file;
	}
	
	public boolean hasUnsavedChanges() {
		return unsavedChanges;
	}

	public void registerSavePerformed() {
		unsavedChanges = false;
		parentTab.setText(synth.getName());
	}
	
	private void registerUnsavedChangePerformed() {
		unsavedChanges = true;
		parentTab.setText(synth.getName() + "*");
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- EDITS ----------------------------------//
	//---------------------------------------------------------------------------//
	
	public void addDevice(ModuleWrapper device, boolean registerToHistory, boolean repaint) {
		addDevices(new ModuleWrapper[]{device}, registerToHistory, repaint);
	}
	
	public void addDevices(ModuleWrapper[] devices, boolean registerToHistory, boolean repaint) {
		for(ModuleWrapper d: devices) {
			//Register parameters change handler
			//TODO When implementing caching to a forwards invalidation of other devices
			d.module.setOnModuleParametersChange(e -> {
				WorldSynthEditorController.instance.updatePreview();
				history.addHistoryEvent(new EditorHistoryEventModuleEdit(d, e.getOldParametersElement(), e.getNewParametersElement()));
				registerUnsavedChangePerformed();
			});
			
			//Register bypass change handler
			//TODO When implementing caching to a forwards invalidation of other devices
			d.module.setOnModuleBypassChange(e -> {
				repaint();
				WorldSynthEditorController.instance.updatePreview();
			});
			
			synth.addDevice(d);
			deviceTree.getChildren().add(new TreeItem<ModuleWrapper>(d));
		}
		
		//Register in history
		if(registerToHistory) {
			history.addHistoryEvent(new EditorHistoryEventModuleAddRemove(devices, null));
		}
		
		registerUnsavedChangePerformed();
		if(repaint) {
			repaint();
		}
	}
	
	public void removeDevice(ModuleWrapper device, boolean registerToHistory, boolean repaint) {
		removeDevices(new ModuleWrapper[]{device}, registerToHistory, repaint);
	}
	
	public void removeDevices(ModuleWrapper[] devices, boolean registerToHistory, boolean repaint) {
		ArrayList<ModuleConnector> removedConnectors = new ArrayList<ModuleConnector>();
		
		for(ModuleWrapper d: devices) {
			//Remove device and add register the removed deviceconnectors
			ModuleConnector[] rc = synth.removeDevice(d);
			//Remove item from tree
			for(TreeItem<ModuleWrapper> item: deviceTree.getChildren()) {
				if(item.getValue() == d) {
					deviceTree.getChildren().remove(item);
					break;
				}
			}
			
			for(ModuleConnector dc: rc) {
				removedConnectors.add(dc);
			}
			//Remove devices from selection
			removeDeviceFromSelection(d);
		}
		
		//Register in history
		if(registerToHistory) {
			//First we concentrate all the subjects into one array
			Object[] subjects = new Object[devices.length + removedConnectors.size()];
			for(int i = 0; i < devices.length; i++) {
				subjects[i] = devices[i];
			}
			for(int i = 0; i < removedConnectors.size(); i++) {
				subjects[devices.length + i] = removedConnectors.get(i);
			}
			history.addHistoryEvent(new EditorHistoryEventModuleAddRemove(null, subjects));
		}
		
		registerUnsavedChangePerformed();
		if(repaint) {
			repaint();
		}
	}
	
	public void addDeviceConnector(ModuleConnector deviceConnector, boolean registerToHistory, boolean repaint) {
		addDeviceConnectors(new ModuleConnector[] {deviceConnector}, registerToHistory, repaint);
	}
	
	public void addDeviceConnectors(ModuleConnector[] deviceConnectors, boolean registerToHistory, boolean repaint) {
		for(ModuleConnector dc: deviceConnectors) {
			synth.addModuleConnector(dc);
		}
		
		//Register in history
		if(registerToHistory) {
			history.addHistoryEvent(new EditorHistoryEventModuleAddRemove(deviceConnectors, null));
		}
		
		registerUnsavedChangePerformed();
		if(repaint) {
			repaint();
		}
	}
	
	public void removeDeviceConnector(ModuleConnector deviceConnector, boolean registerToHistory, boolean repaint) {
		removeDeviceConnectors(new ModuleConnector[] {deviceConnector}, registerToHistory, repaint);
	}
	
	public void removeDeviceConnectors(ModuleConnector[] deviceConnectors, boolean registerToHistory, boolean repaint) {
		for(ModuleConnector dc: deviceConnectors) {
			synth.removeModuleConnector(dc);
		}
		
		//Register in history
		if(registerToHistory) {
			history.addHistoryEvent(new EditorHistoryEventModuleAddRemove(null, deviceConnectors));
		}
		
		registerUnsavedChangePerformed();
		if(repaint) {
			repaint();
		}
	}
	
	public void setTempDevice(ModuleWrapper tempDevice) {
		this.tempWrapper = tempDevice;
		if(tempDevice != null) {
			tempDevice.posX = mouseListener.currentMouseCoordinateX;
			tempDevice.posY = mouseListener.currentMouseCoordinateY;
		}
		repaint();
	}
	
	public void setTempSynth(TempSynth tempSynth) {
		this.tempSynth = tempSynth;
		if(tempSynth != null) {
			tempSynth.setCenterTo(mouseListener.currentMouseCoordinateX, mouseListener.currentMouseCoordinateY);
		}
		repaint();
	}
	
	private void applyTempDevice() {
		addDevice(tempWrapper, true, true);
		//Instance a new device of same type
		try {
			tempWrapper = new ModuleWrapper(tempWrapper.module.getClass(), getSynth(), tempWrapper.posX, tempWrapper.posY);
		} catch(Exception e) {
			e.printStackTrace();
			tempWrapper = null;
		}
	}
	
	private void applyTempSynth() {
		ArrayList<Object> added = new ArrayList<Object>();
		
		for(ModuleWrapper d: tempSynth.getWrapperList()) {
			addDevice(d, false, false);
			added.add(d);
		}
		for(ModuleConnector dc: tempSynth.getModuleConnectorList()) {
			addDeviceConnector(dc, false, false);
			added.add(dc);
		}
		tempSynth.reinstance(getSynth());
		
		//Register in history
		history.addHistoryEvent(new EditorHistoryEventModuleAddRemove(added.toArray(new Object[0]), null));
		
		registerUnsavedChangePerformed();
		repaint();
	}
	
	public void openDeviceParametersEditor(ModuleWrapper device) {
		new ModuleParametersStage(device);
	}
	
	public void renameDevice(ModuleWrapper device, String customName) {
		device.setCustomName(customName);
		WorldSynthEditorController.instance.synthTree.refresh();
		repaint();
		
		//TODO register renaming to history
	}
	
	//---------------------------------------------------------------------------//
	//----------------------------- EDITS SELECTION -----------------------------//
	//---------------------------------------------------------------------------//
	
	public ModuleWrapper[] getSelectedWrappers() {
		ModuleWrapper[] devices = new ModuleWrapper[0];
		devices = selectedWrappers.toArray(devices);
		return devices;
	}
	
	public ModuleWrapper getLatestSelectedWrapper() {
		if(selectedWrappers.size() == 0) {
			return null;
		}
		return selectedWrappers.get(selectedWrappers.size() - 1);
	}
	
	public void setSelectedWrapper(ModuleWrapper wrapper) {
		if(wrapper == null) {
			return;
		}
		
		setSelectedWrappers(new ModuleWrapper[]{wrapperOver}, false);
		//Update preview
		WorldSynthEditorController.instance.updatePeview(synth, getLatestSelectedWrapper());
		
		repaint();
	}
	
	private boolean ignoreSelectionCallFromTreeView = false;
	public void setSelectedWrappers(ModuleWrapper[] wrappers, boolean calledFromTreview) {
		if(calledFromTreview && ignoreSelectionCallFromTreeView) {
			return;
		}
		
		//TODO check if new selection has single new selection and use that for updating preview in that case
		
		//Check that the list of devices is not empty
		if(wrappers.length == 0) {
			return;
		}
		//Check that the devices is part of the synth being edited in this editor
		for(ModuleWrapper d: wrappers) {
			if(!synth.containsWrapper(d)) {
				return;
			}
		}
		
		//If it contains all the devices set the selection
		//Clear selection and add the device as the only entry
		selectedWrappers.clear();
		for(ModuleWrapper d: wrappers) {
			selectedWrappers.add(d);
		}
		
		if(calledFromTreview) { 
			//Update preview
			WorldSynthEditorController.instance.updatePeview(synth, getLatestSelectedWrapper());
		}
		else {
			//Update tree view with latest selected devices
			updateTreeSelection(getSelectedWrappers());
		}
		
		repaint();
	}
	
	public void toggleDeviceSelection(ModuleWrapper device) {
		//Check that the device is part of the synth being edited in this editor
		if(synth.containsWrapper(device)) {
			//Check if the device is already contained in the selection
			//If the device already is part of the selection remove it so the instance only is contained once when added at the end
			boolean updatePreview = false;
			if(selectedWrappers.contains(device)) {
				if(getLatestSelectedWrapper() == device) {
					selectedWrappers.remove(device);
				}
				else {
					selectedWrappers.remove(device);
					selectedWrappers.add(device);
					updatePreview = true;
				}
			}
			else {
				selectedWrappers.add(device);
				updatePreview = true;
			}
			
			//Update tree view with latest selected devices
			updateTreeSelection(getSelectedWrappers());
			//Update preview
			if(updatePreview) {
				WorldSynthEditorController.instance.updatePeview(synth, getLatestSelectedWrapper());
			}
			
			repaint();
		}
	}
	
	public void removeDeviceFromSelection(ModuleWrapper device) {
		selectedWrappers.remove(device);
		
		repaint();
		
		//Update tree view with latest selected devices
		updateTreeSelection(getSelectedWrappers());
	}
	
	public void clearSelectedDevices() {
		selectedWrappers.clear();
		
		repaint();
		
		//Update tree view with latest selected devices
		updateTreeSelection(getSelectedWrappers());
	}
	
	private void updateTreeSelection(ModuleWrapper[] selectedDevices) {
		//Update tree selection
		ignoreSelectionCallFromTreeView = true;
		WorldSynthEditorController.instance.synthTree.setSelection(selectedDevices);
		ignoreSelectionCallFromTreeView = false;
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- PAINT ----------------------------------//
	//---------------------------------------------------------------------------//
	
	public void repaint() {
		paintEditor();
	}
	
	private void paintEditor() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(Color.web("#303030"));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(1.0, zoom*2-0.5))));
		g.setLineWidth(1.0);
		float gridIncrement = 50;
		for(float cx = gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x < getWidth(); cx += gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cx = -gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x > 0; cx -= gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cy = gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y < getHeight(); cy += gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		for(float cy = -gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y > 0; cy -= gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		
		//Draw center cross;
		g.setStroke(Color.web("#606060"));
		Pixel centerCoordinatePixel = new Pixel(new Coordinate(0, 0), this);
		g.strokeLine(centerCoordinatePixel.x, 0, centerCoordinatePixel.x, getHeight());
		g.strokeLine(0, centerCoordinatePixel.y, getWidth(), centerCoordinatePixel.y);
		
		//Draw devices
		for(ModuleWrapper d: synth.getWrapperList()) {
			paintDevice(d, g);
		}
		
		//Draw deviceconnectors
		for(ModuleConnector dc: synth.getModuleConnectorList()) {
			paintDeviceConnector(dc, g);
		}
		
		//Draw the temp device
		if(tempWrapper != null) {
			paintDevice(tempWrapper, g);
		}
		
		//Draw the temp deviceconnector
		if(tempConnector != null) {
			paintDeviceConnector(tempConnector, g);
		}
		
		//Draw the tempsynth
		if(tempSynth != null) {
			for(ModuleWrapper d: tempSynth.getWrapperList()) {
				paintDevice(d, g);
			}
			for(ModuleConnector dc: tempSynth.getModuleConnectorList()) {
				paintDeviceConnector(dc, g);
			}
		}
		
		//Draw the selectionrectangle
		if(selectionRectangle != null) {
			paintSelectionRectangle(selectionRectangle, g);
		}
		
		//Draw the selectionlasso
		if(selectionLasso != null) {
			paintSelectionLasso(selectionLasso, g);
		}
		
		//TODO Draw more when implementing grouping, comments...
	}
	
	private void paintDevice(ModuleWrapper wrapper, GraphicsContext g) {
		float ioRenderSize = ModuleWrapperIO.IO_RENDERSIZE;
		
		Color deviceColor = wrapper.module.getModuleColor();
		//If device is bypasset it is rendered gray
		if(wrapper.isBypassed()) {
			deviceColor = Color.DIMGRAY;
		}
		Color deviceOverColor = deviceColor.darker();
		Color deviceStrokeColor = Color.BLACK;
		Color deviceOverStrokeColor = Color.MAGENTA;
		
		Color deviceIoStrokeColor = Color.BLACK;
		Color deviceIoOverStrokeColor = Color.MAGENTA;
		
		Coordinate coord = new Coordinate(wrapper.posX, wrapper.posY);
		Pixel pixel = new Pixel(coord, this);
		
		g.setLineWidth(1.0);
		
		//Draw selection behind
		if(selectedWrappers.contains(wrapper)) {
			g.setFill(Color.WHITE);
			g.setEffect(new BoxBlur(10.0*zoom, 10.0*zoom, 3));
			g.fillRoundRect(pixel.x-2.0*zoom, pixel.y-2.0*zoom, (wrapper.wrapperWidth+4.0)*zoom, (wrapper.wrapperHeight+4.0)*zoom, 12.0*zoom, 12.0*zoom);
			g.setEffect(null);
		}
		if(wrapper == WorldSynthEditorController.instance.currentPreviewDevice) {
			g.setFill(Color.YELLOW);
			g.setEffect(new BoxBlur(10.0*zoom, 10.0*zoom, 3));
			g.fillRoundRect(pixel.x-2.0*zoom, pixel.y-2.0*zoom, (wrapper.wrapperWidth+4.0)*zoom, (wrapper.wrapperHeight+4.0)*zoom, 12.0*zoom, 12.0*zoom);
			g.setEffect(null);
		}
		
		//Draw main device rect;
		g.setFill(deviceColor);
		g.setStroke(deviceStrokeColor);
		if(wrapperOver == wrapper && wrapperIoOver == null) {
			g.setFill(deviceOverColor);
			g.setStroke(deviceOverStrokeColor);
		}
		g.fillRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*zoom, wrapper.wrapperHeight*zoom, 10.0*zoom, 10.0*zoom);
//		g.fillRect(pixel.x, pixel.y, device.deviceWidth*zoom, device.deviceHeight*zoom);
		g.strokeRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*zoom, wrapper.wrapperHeight*zoom, 10.0*zoom, 10.0*zoom);
//		g.strokeRect(pixel.x, pixel.y, device.deviceWidth*zoom, device.deviceHeight*zoom);
		
		//Draw device IO inputs
		if(wrapper.module.getInputs() != null) {
			for(ModuleWrapperIO io: wrapper.wrapperInputs.values()) {
				if(!io.getIO().isVisible() && !(wrapper.module instanceof ModuleMacro)) {
					continue;
				}
				else if(io.getDatatype() instanceof DatatypeMultitype) {
					//Draw multitype io with several colors in several vertical stripes
					DatatypeMultitype multitype = (DatatypeMultitype)io.getDatatype();
					int typeCount = multitype.getDatatypes().length;
					for(int i = 0; i < typeCount; i++) {
						g.setFill(multitype.getDatatypes()[i].getDatatypeColor());
						if(io == wrapperIoOver) {
							g.setFill(multitype.getDatatypes()[i].getDatatypeColor().darker());
						}
						g.fillRect(pixel.x + io.posX*zoom, pixel.y + ioRenderSize/typeCount*zoom*i + io.posY*zoom, ioRenderSize*zoom, ioRenderSize/typeCount*zoom);
					}
				}
				else {
					//Draw normal io with one color
					g.setFill(io.getDatatype().getDatatypeColor());
					if(io == wrapperIoOver) {
						g.setFill(io.getDatatype().getDatatypeColor().darker());
					}
					g.fillRect(pixel.x + io.posX*zoom, pixel.y + io.posY*zoom, ioRenderSize*zoom, ioRenderSize*zoom);
				}
				
				g.setStroke(io == wrapperIoOver ? deviceIoOverStrokeColor : deviceIoStrokeColor);
				g.strokeRect(pixel.x + io.posX*zoom, pixel.y + io.posY*zoom, ioRenderSize*zoom, ioRenderSize*zoom);
			}
		}
		
		//Draw device IO outputs
		if(wrapper.module.getOutputs() != null) {
			for(ModuleWrapperIO io: wrapper.wrapperOutputs.values()) {
				if(!io.getIO().isVisible() && !(wrapper.module instanceof ModuleMacro)) {
					continue;
				}
				else if(io.getDatatype() instanceof DatatypeMultitype) {
					//Draw multitype io with several colors in several vertical stripes
					DatatypeMultitype multitype = (DatatypeMultitype)io.getDatatype();
					int typeCount = multitype.getDatatypes().length;
					for(int i = 0; i < typeCount; i++) {
						g.setFill(multitype.getDatatypes()[i].getDatatypeColor());
						if(io == wrapperIoOver) {
							g.setFill(multitype.getDatatypes()[i].getDatatypeColor().darker());
						}
						g.fillRect(pixel.x + io.posX*zoom, pixel.y + ioRenderSize/typeCount*zoom*i + io.posY*zoom, ioRenderSize*zoom, ioRenderSize/typeCount*zoom);
					}
				}
				else {
					//Draw normal io with one color
					g.setFill(io.getDatatype().getDatatypeColor());
					if(io == wrapperIoOver) {
						g.setFill(io.getDatatype().getDatatypeColor().darker());
					}
					g.fillRect(pixel.x + io.posX*zoom, pixel.y + io.posY*zoom, ioRenderSize*zoom, ioRenderSize*zoom);
				}
				
				g.setStroke(io == wrapperIoOver ? deviceIoOverStrokeColor : deviceIoStrokeColor);
				g.strokeRect(pixel.x + io.posX*zoom, pixel.y + io.posY*zoom, ModuleWrapperIO.IO_RENDERSIZE*zoom,ioRenderSize*zoom);
			}
		}
		
		//Draw device bypass
		if(wrapper.isBypassed()) {
			g.setStroke(Color.RED);
			g.setLineWidth(zoom*2);
			//Draw line between main input and main output
			Pixel mi = new Pixel(pixel.x + ioRenderSize*zoom/2.0, pixel.y + (10.0+ioRenderSize/2.0)*zoom);
			Pixel mo = new Pixel(pixel.x + (wrapper.wrapperWidth-10.0)*zoom + ioRenderSize*zoom/2.0, pixel.y + (10.0+ioRenderSize/2.0)*zoom);
			g.strokeLine(mi.x, mi.y, mi.x+(int) (10*zoom), mi.y+(int) (-10*zoom));
			g.strokeLine(mi.x+(int) (10*zoom), mi.y+(int) (-10*zoom), mo.x-(int) (10*zoom), mo.y+(int) (-10*zoom));
			g.strokeLine(mo.x-(int) (10*zoom), mo.y+(int) (-10*zoom), mo.x, mo.y);
		}
		
		//Draw text
		g.setFill(Color.WHITE);
		g.setFont(new Font("SansSerif", 15.0*zoom));
		g.fillText(wrapper.toString(), pixel.x, pixel.y - 5*zoom);
		
		//Draw a string to indicate the io name if hvering over one
		if(wrapperOver == wrapper && wrapperIoOver != null) {
			g.setFill(Color.GRAY);
			g.fillText(wrapperIoOver.toString(), pixel.x, pixel.y + (wrapper.wrapperHeight + 15)*zoom);
		}
	}
	
	private void paintDeviceConnector(ModuleConnector connector, GraphicsContext g) {
		float ioRenderSize = ModuleWrapperIO.IO_RENDERSIZE;
		double startX = 0;
		double startY = 0;
		double endX = 0;
		double endY = 0;
		
		if(connector.outputWrapper != null && connector.outputWrapperIo != null) {
			double x = connector.outputWrapper.posX + connector.outputWrapperIo.posX + ioRenderSize/2.0;
			double y = connector.outputWrapper.posY + connector.outputWrapperIo.posY + ioRenderSize/2.0;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), this);
			startX = pixel.x;
			startY = pixel.y;
		}
		else {
			startX = mouseListener.currentEvent.getX();
			startY = mouseListener.currentEvent.getY();
		}
		
		if(connector.inputWrapper != null && connector.inputWrapperIo != null) {
			double x = connector.inputWrapper.posX + connector.inputWrapperIo.posX + ioRenderSize/2.0;
			double y = connector.inputWrapper.posY + connector.inputWrapperIo.posY + ioRenderSize/2.0;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), this);
			endX = pixel.x;
			endY = pixel.y;
		}
		else {
			endX = mouseListener.currentEvent.getX();
			endY = mouseListener.currentEvent.getY();
		}
		
		//Draw ball at the connection point
		g.setLineCap(StrokeLineCap.ROUND);
		g.setFill(Color.LIGHTGRAY);
		g.setStroke(Color.LIGHTGRAY);
		g.setLineWidth(2*zoom);
		g.strokeLine(startX, startY, startX+30*zoom, startY);
		g.strokeLine(startX+30*zoom, startY, endX-30*zoom, endY);
		g.strokeLine(endX-30*zoom, endY, endX, endY);
		g.setFill(Color.DIMGRAY);
		g.setStroke(Color.DIMGRAY);
		if(connector.outputWrapper != null) g.fillOval(startX-3*zoom, startY-3*zoom, 6*zoom, 6*zoom);
		if(connector.inputWrapper != null) g.fillOval(endX-3*zoom, endY-3*zoom, 6*zoom, 6*zoom);
	}
	
	private void paintSelectionRectangle(SelectionRectangle selectionRectangle, GraphicsContext g) {
		Coordinate startCoord = new Coordinate(selectionRectangle.getX1(), selectionRectangle.getY1());
		Pixel startPixel = new Pixel(startCoord, this);
		Coordinate endCoord = new Coordinate(selectionRectangle.getX2(), selectionRectangle.getY2());
		Pixel endPixel = new Pixel(endCoord, this);
		
		g.setStroke(Color.WHITE);
		g.setLineWidth(2.0);
		g.setLineDashes(10.0, 10.0);
		g.strokeRect(startPixel.x, startPixel.y, endPixel.x-startPixel.x, endPixel.y-startPixel.y);
		g.setLineDashes();
	}
	
	private void paintSelectionLasso(SelectionLasso selectionLasso, GraphicsContext g) {
		double[][] lassoPoints = selectionLasso.getPoints();
		double[] xPoints = new double[lassoPoints.length];
		double[] yPoints = new double[lassoPoints.length];
		for(int i = 0; i < selectionLasso.getPoints().length; i++) {
			Coordinate coord = new Coordinate(lassoPoints[i][0], lassoPoints[i][1]);
			Pixel pixel = new Pixel(coord, this);
			xPoints[i] = pixel.x;
			yPoints[i] = pixel.y;
		}
		
		g.setStroke(Color.WHITE);
		g.setLineWidth(2.0);
		g.setLineDashes(10.0, 10.0);
		g.strokePolygon(xPoints, yPoints, lassoPoints.length);
		g.setLineDashes();
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- EVENT ----------------------------------//
	//---------------------------------------------------------------------------//
	
	private class MouseListener implements EventHandler<MouseEvent> {
		public MouseEvent currentEvent;
		
		public double lastMouseCoordinateX, lastMouseCoordinateY;
		public double currentMouseCoordinateX, currentMouseCoordinateY;
		public boolean mouseOverEditor = false;
		
		private EditorHistoryEventModuleMove tempHistoryEventMove = null;
		
		ArrayList<MouseAction<MouseListener>> mouseActions = new ArrayList<MouseAction<MouseListener>>();
		
		public MouseListener() {
			
			mouseActions.add(new MouseMoveAction());
			mouseActions.add(new MoveViewAction());
			mouseActions.add(new MoveModuleAction());
			mouseActions.add(new EndModuleMoveAction());
			
			mouseActions.add(new OpenModuleSelectionContextMenuAction());
			mouseActions.add(new OpenModuleEditContextMenuAction());
			
			mouseActions.add(new ModuleSelectionAction());
			mouseActions.add(new EditModuleParametersAction());
			
			mouseActions.add(new MoveTempModuleAction());
			mouseActions.add(new MoveTempSynthAction());
			mouseActions.add(new ApplyTempModuleAction());
			mouseActions.add(new ApplyTempSynthAction());
			mouseActions.add(new CancelTempModuleAction());
			mouseActions.add(new CancelTempSynthAction());
			mouseActions.add(new StartConnectorAction());
			mouseActions.add(new EndConnectorAction());
			mouseActions.add(new CancelConnectorAction());
			
			mouseActions.add(new StartAreaSelectionAction());
			mouseActions.add(new UpdateAreaSelectionAction());
			mouseActions.add(new EndAreaSelectionAction());
		}
		
		@Override
		public void handle(MouseEvent event) {
			currentEvent = event;
			
			if(event.getEventType() == MouseEvent.MOUSE_CLICKED && !event.isStillSincePress()) {
				//If a click was not still it's not regarded as a click and should be ignored... PERIOD
				return;
			}
			if(event.getEventType() == MouseEvent.MOUSE_DRAGGED && event.isStillSincePress()) {
				return;
			}
			if(event.getEventType() == MouseEvent.MOUSE_EXITED) {
				wrapperOver = null;
				wrapperIoOver = null;
				mouseOverEditor = false;
				repaint();
			}
			if(event.getEventType() == MouseEvent.MOUSE_ENTERED) {
				mouseOverEditor = true;
				repaint();
			}
			
			lastMouseCoordinateX = currentMouseCoordinateX;
			lastMouseCoordinateY = currentMouseCoordinateY;
			
			//Update mouse coodinate position
			Coordinate mouseCoordinate = new Coordinate(new Pixel(event.getX(), event.getY()), SynthEditorPane.this);
			currentMouseCoordinateX = mouseCoordinate.x;
			currentMouseCoordinateY = mouseCoordinate.y;
			
			fireAppropriateAction();
		}
		
		private void fireAppropriateAction() {
			for(MouseAction<MouseListener> action: mouseActions) {
				if(validateFilter(action.getActionFilter())) {
					action.action(this);
				}
			}
		}
		
		private boolean validateFilter(EditorMouseInputFilter filter) {
			if(filter.mouseEventType != currentEvent.getEventType()) {
				return false;
			}
			else if(filter.mouseButton != currentEvent.getButton()) {
				return false;
			}
			else if(filter.mouseClickCount > 0 && filter.mouseClickCount != currentEvent.getClickCount()) {
				return false;
			}
			
			
			
			else if(filter.mouseOverModule != InputState.UNUSED && filter.mouseOverModule.getState() != (wrapperOver != null)) {
				return false;
			}
			else if(filter.mouseOverIo != InputState.UNUSED && filter.mouseOverIo.getState() != (wrapperIoOver != null)) {
				return false;
			}
			else if(filter.tempModule != InputState.UNUSED && filter.tempModule.getState() != (tempWrapper != null)) {
				return false;
			}
			else if(filter.tempConnector != InputState.UNUSED && filter.tempConnector.getState() != (tempConnector != null)) {
				return false;
			}
			else if(filter.tempSynth != InputState.UNUSED && filter.tempSynth.getState() != (tempSynth != null)) {
				return false;
			}
			else if(filter.areaSelector != InputState.UNUSED && filter.areaSelector.getState() != (selectionRectangle != null || selectionLasso != null)) {
				return false;
			}
			
			return true;
		}
	}
	
	private class KeyboardListener implements EventHandler<KeyEvent> {
		public KeyEvent currentEvent;
		
		ArrayList<KeyboardAction<KeyboardListener>> keyActions = new ArrayList<KeyboardAction<KeyboardListener>>();
		
		public KeyboardListener() {
			keyActions.add(new DeleteModuleAction());
			keyActions.add(new DeviceRenameAction());
			keyActions.add(new DeviceBypassAction());
			keyActions.add(new DeviceEditAction());
			keyActions.add(new DeviceSearchAction());
			keyActions.add(new UndoAction());
			keyActions.add(new RedoAction());
			keyActions.add(new EscapeAction());
			keyActions.add(new CutAction());
			keyActions.add(new CopyAction());
			keyActions.add(new PasteAction());
		}
		
		@Override
		public void handle(final KeyEvent event) {
			if(!mouseListener.mouseOverEditor) {
//				return;
			}
			if(deviceSearchPopup.isShowing()) {
				//Ignore keyboard actions if device search is open
				return;
			}
			currentEvent = event;
			
			fireAppropriateAction(event);
		}
		
		private void fireAppropriateAction(final KeyEvent event) {
			for(KeyboardAction<KeyboardListener> action: keyActions) {
				if(validateFilters(action.getActionFilters(), event)) {
					action.action(this);
				}
			}
		}
		
		private boolean validateFilters(final EditorKeyboardInputFilter[] filters, final KeyEvent event) {
			for(EditorKeyboardInputFilter filter: filters) {
				if(validateFilter(filter, event)) {
					return true;
				}
			}
			return false;
		}
		
		private boolean validateFilter(final EditorKeyboardInputFilter filter, final KeyEvent event) {
			if(filter.keyEventType != event.getEventType()) {
				return false;
			}
			else if(filter.keyCode != event.getCode()) {
				return false;
			}
			else if(filter.modifierCtrl != InputState.UNUSED && filter.modifierCtrl.getState() != event.isShortcutDown()) {
				return false;
			}
			else if(filter.modifierShift != InputState.UNUSED && filter.modifierShift.getState() != event.isShiftDown()) {
				return false;
			}
			else if(filter.modifierAlt != InputState.UNUSED && filter.modifierAlt.getState() != event.isAltDown()) {
				return false;
			}
			
			else if(filter.mouseOverModule != InputState.UNUSED && filter.mouseOverModule.getState() != (wrapperOver != null)) {
				return false;
			}
			else if(filter.mouseOverIo != InputState.UNUSED && filter.mouseOverIo.getState() != (wrapperIoOver != null)) {
				return false;
			}
			else if(filter.tempModule != InputState.UNUSED && filter.tempModule.getState() != (tempWrapper != null)) {
				return false;
			}
			else if(filter.tempConnector != InputState.UNUSED && filter.tempConnector.getState() != (tempConnector != null)) {
				return false;
			}
			else if(filter.tempSynth != InputState.UNUSED && filter.tempSynth.getState() != (tempSynth != null)) {
				return false;
			}
			else if(filter.areaSelector != InputState.UNUSED && filter.areaSelector.getState() != (selectionRectangle != null || selectionLasso != null)) {
				return false;
			}
			
			return true;
		}
	}
	
	public EventHandler<KeyEvent> getKeyboardListener() {
		return keyboardListener;
	}
	//---------------------------------------------------------------------------//
	//------------------------------ MOUSE ACTIONS ------------------------------//
	//---------------------------------------------------------------------------//
	
	private class MouseMoveAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_MOVED, MouseButton.NONE, 0, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			//Is mouse over a device
			boolean foundWrapper = false;
			boolean foundWrapperIo = false;
			ModuleWrapper lastWrapper = wrapperOver;
			ModuleWrapperIO lastWrapperIo = wrapperIoOver;
			for(ModuleWrapper d: synth.getWrapperList()) {
				if(t.currentMouseCoordinateX >= d.posX && t.currentMouseCoordinateX <= d.posX+d.wrapperWidth && t.currentMouseCoordinateY >= d.posY && t.currentMouseCoordinateY <= d.posY+d.wrapperHeight) {
					wrapperOver = d;
					foundWrapper = true;
					
					//Is mouse over an IO
					if(d.wrapperInputs.size() > 0) {
						for(ModuleWrapperIO io: d.wrapperInputs.values()) {
							if(!io.getIO().isVisible()) {
								continue;
							}
							if(t.currentMouseCoordinateX >= d.posX+io.posX && t.currentMouseCoordinateX <= d.posX+io.posX+ModuleWrapperIO.IO_RENDERSIZE && t.currentMouseCoordinateY >= d.posY+io.posY && t.currentMouseCoordinateY <= d.posY+io.posY+ModuleWrapperIO.IO_RENDERSIZE) {
								wrapperIoOver = io;
								foundWrapperIo = true;
								break;
							}
						}
					}
					if(d.wrapperOutputs.size() > 0) {
						for(ModuleWrapperIO io: d.wrapperOutputs.values()) {
							if(!io.getIO().isVisible()) {
								continue;
							}
							if(t.currentMouseCoordinateX >= d.posX+io.posX && t.currentMouseCoordinateX <= d.posX+io.posX+ModuleWrapperIO.IO_RENDERSIZE && t.currentMouseCoordinateY >= d.posY+io.posY && t.currentMouseCoordinateY <= d.posY+io.posY+ModuleWrapperIO.IO_RENDERSIZE) {
								wrapperIoOver = io;
								foundWrapperIo = true;
								break;
							}
						}
					}
				}
			}
			if(!foundWrapper) {
				wrapperOver = null;
			}
			if(!foundWrapperIo) {
				wrapperIoOver = null;
			}
			if(wrapperOver != lastWrapper || wrapperIoOver != lastWrapperIo || tempConnector != null) {
				repaint();
			}
		}
	}
	
	private class MoveViewAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_DRAGGED, MouseButton.SECONDARY, 0, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			double diffX = t.lastMouseCoordinateX - t.currentMouseCoordinateX;
			double diffY = t.lastMouseCoordinateY - t.currentMouseCoordinateY;
			
			t.currentMouseCoordinateX += diffX;
			t.currentMouseCoordinateY += diffY;
			
			centerCoordX += diffX;
			centerCoordY += diffY;
			
			repaint();
		}
	}
	
	private class ModuleSelectionAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 1, InputState.YES, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			if(t.currentEvent.isShortcutDown()) {
				toggleDeviceSelection(wrapperOver);
			}
			else {
				setSelectedWrapper(wrapperOver);
			}
		}
	}
	
	private class MoveModuleAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_DRAGGED, MouseButton.PRIMARY, 0, InputState.YES, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			double diffX = t.lastMouseCoordinateX - t.currentMouseCoordinateX;
			double diffY = t.lastMouseCoordinateY - t.currentMouseCoordinateY;
			
			//Create a move event with the old coordinates at first move
			if(t.tempHistoryEventMove == null) {
				//Move multiple selected
				if(selectedWrappers.contains(wrapperOver) && !t.currentEvent.isShortcutDown()) {
					Object[] moveSubjects = new Object[selectedWrappers.size()];
					Coordinate[] oldCoordinates = new Coordinate[selectedWrappers.size()];
					Coordinate[] newCoordinates = new Coordinate[selectedWrappers.size()];
					for(int i = 0; i < selectedWrappers.size(); i++) {
						moveSubjects[i] = selectedWrappers.get(i);
						oldCoordinates[i] = new Coordinate(selectedWrappers.get(i).posX, selectedWrappers.get(i).posY);
						newCoordinates[i] = new Coordinate(selectedWrappers.get(i).posX, selectedWrappers.get(i).posY);
					}
					t.tempHistoryEventMove = new EditorHistoryEventModuleMove(moveSubjects, oldCoordinates, newCoordinates);
				}
				//Move single under cursor
				else {
					Object[] moveSubjects = {wrapperOver};
					Coordinate[] oldCoordinates = {new Coordinate(wrapperOver.posX, wrapperOver.posY)};
					Coordinate[] newCoordinates = {new Coordinate(wrapperOver.posX, wrapperOver.posY)};
					t.tempHistoryEventMove = new EditorHistoryEventModuleMove(moveSubjects, oldCoordinates, newCoordinates);
				}
			}
			
			Object[] moveSubjects = t.tempHistoryEventMove.getMoveSubjects();
			for(int i = 0; i < moveSubjects.length; i++) {
				//Move device
				ModuleWrapper subject = (ModuleWrapper) moveSubjects[i];
				subject.posX -= diffX;
				subject.posY -= diffY;
				
				//Update latest position for history event
				t.tempHistoryEventMove.getNewCoordinates()[i].x = subject.posX;
				t.tempHistoryEventMove.getNewCoordinates()[i].y = subject.posY;
			}
			
			repaint();
		}
	}
	
	private class EndModuleMoveAction implements MouseAction<MouseListener> {

		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_RELEASED, MouseButton.PRIMARY, 0, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED);
		}

		@Override
		public void action(MouseListener t) {
			if(t.tempHistoryEventMove != null) {
				history.addHistoryEvent(t.tempHistoryEventMove);
				t.tempHistoryEventMove = null;
				registerUnsavedChangePerformed();
			}
		}
		
	}
	
	private class StartConnectorAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 1, InputState.UNUSED, InputState.YES, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			if(wrapperIoOver.isInput()) {
				ModuleConnector[] mc = synth.getConnectorsByWrapperIo(wrapperIoOver);
				if(mc.length > 0) {
					for(ModuleConnector c: mc) {
						removeDeviceConnector(c, true, true);
					}
					repaint();
				}
				tempConnector = new ModuleConnector(null, null, wrapperOver, wrapperIoOver);
			}
			else {
				tempConnector = new ModuleConnector(wrapperOver, wrapperIoOver, null, null);
			}
		}
	}
	
	private class EndConnectorAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 1, InputState.UNUSED, InputState.YES, InputState.NO, InputState.YES, InputState.NO, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			if(tempConnector.inputWrapper != null && tempConnector.inputWrapperIo != null && !wrapperIoOver.isInput() && tempConnector.inputWrapper != wrapperOver) {
				//If the mouse is over an output that is not on the already connected device and the connected io is an input
				tempConnector.setOutputWrapper(wrapperOver, wrapperIoOver);
				if(tempConnector.verifyInputOutputType()) {
					addDeviceConnector(tempConnector, true, true);
				}
				tempConnector = null;
			}
			else if(tempConnector.outputWrapper != null && tempConnector.outputWrapperIo != null && wrapperIoOver.isInput() && tempConnector.outputWrapper != wrapperOver) {
				//If the mouse is over an input that is not on the already connected device and the connected io is an output
				ModuleConnector[] c = synth.getConnectorsByWrapperIo(wrapperIoOver);
				
				tempConnector.setInputWrapper(wrapperOver, wrapperIoOver);
				
				if(tempConnector.verifyInputOutputType()) {
					if(c.length > 0) {
						//If there is already a connector on the input remove it so it gets replaced
						for(ModuleConnector dc: c) {
							removeDeviceConnector(dc, true, false);
						}
					}
					addDeviceConnector(tempConnector, true, true);
				}
				tempConnector = null;
			}
		}
	}
	
	private class CancelConnectorAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.SECONDARY, 1, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.YES, InputState.NO, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			tempConnector = null;
			repaint();
		}
	}
	
	private class CancelTempModuleAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.SECONDARY, 1, InputState.UNUSED, InputState.UNUSED, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			setTempDevice(null);
		}
	}
	
	private class CancelTempSynthAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.SECONDARY, 1, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.YES, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			setTempSynth(null);
		}
	}
	
	private class EditModuleParametersAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 2, InputState.YES, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			if(wrapperOver.module instanceof ModuleMacro) {
				WorldSynthEditorController.instance.openSynthEditor(((ModuleMacro)wrapperOver.module).getMacroSynth(), null);
			}
			else {
				openDeviceParametersEditor(wrapperOver);
			}
		}
	}

	private class ApplyTempModuleAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 1, InputState.UNUSED, InputState.UNUSED, InputState.YES, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			applyTempDevice();
		}
	}
	
	private class ApplyTempSynthAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.PRIMARY, 1, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.YES, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			applyTempSynth();
		}
	}
	
	private class MoveTempModuleAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_MOVED, MouseButton.NONE, 0, InputState.UNUSED, InputState.UNUSED, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			tempWrapper.posX = t.currentMouseCoordinateX - tempWrapper.wrapperWidth/2;
			tempWrapper.posY = t.currentMouseCoordinateY - tempWrapper.wrapperHeight/2;
			repaint();
		}
	}

	private class MoveTempSynthAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_MOVED, MouseButton.NONE, 0, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.YES, InputState.UNUSED);
		}
		
		@Override
		public void action(MouseListener t) {
			tempSynth.setCenterTo(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
			repaint();
		}
	}
	
	private class OpenModuleSelectionContextMenuAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.SECONDARY, 1, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			ModuleSelectionContextMenu contextMenu = new ModuleSelectionContextMenu(SynthEditorPane.this);
			contextMenu.show(SynthEditorPane.this, t.currentEvent.getScreenX(), t.currentEvent.getScreenY());
		}
	}
	
	private class OpenModuleEditContextMenuAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_CLICKED, MouseButton.SECONDARY, 1, InputState.YES, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			ModuleEditContextMenu contextMenu = new ModuleEditContextMenu(wrapperOver, SynthEditorPane.this);
			contextMenu.show(SynthEditorPane.this, t.currentEvent.getScreenX(), t.currentEvent.getScreenY());
		}
	}
	
	private class StartAreaSelectionAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_DRAGGED, MouseButton.PRIMARY, 0, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO, InputState.NO);
		}
		
		@Override
		public void action(MouseListener t) {
			if(t.currentEvent.isShortcutDown()) {
				selectionLasso = new SelectionLasso(t.lastMouseCoordinateX, t.lastMouseCoordinateY);
				selectionLasso.setCurrentCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
			}
			else {
				selectionRectangle = new SelectionRectangle(t.lastMouseCoordinateX, t.lastMouseCoordinateY);
				selectionRectangle.setEndCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
			}
			repaint();
		}
	}
	
	private class UpdateAreaSelectionAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_DRAGGED, MouseButton.PRIMARY, 0, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.YES);
		}
		
		@Override
		public void action(MouseListener t) {
			if(selectionLasso != null) {
				selectionLasso.setCurrentCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
			}
			else {
				selectionRectangle.setEndCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
			}
			repaint();
		}
	}
	
	private class EndAreaSelectionAction implements MouseAction<MouseListener> {
		
		@Override
		public EditorMouseInputFilter getActionFilter() {
			return new EditorMouseInputFilter(MouseEvent.MOUSE_RELEASED, MouseButton.PRIMARY, 0, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.YES);
		}
		
		@Override
		public void action(MouseListener t) {
			if(selectionRectangle != null) {
				setSelectedWrappers(selectionRectangle.getDevicesInside(SynthEditorPane.this), false);
				selectionRectangle = null;
			}
			else {
				setSelectedWrappers(selectionLasso.getDevicesInside(SynthEditorPane.this), false);
				selectionLasso = null;
			}
			repaint();
		}
	}

	//---------------------------------------------------------------------------//
	//------------------------------- KEY ACTIONS -------------------------------//
	//---------------------------------------------------------------------------//
	
	private class DeleteModuleAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.DELETE, InputState.NO, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO),
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.BACK_SPACE, InputState.NO, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
			};
		}
		
		@Override
		public void action(KeyboardListener t) {
			removeDevices(selectedWrappers.toArray(new ModuleWrapper[0]), true, true);
			
//			WorldSynthMainUI.synthBrowser.setBrowserContent(synth);
			clearSelectedDevices();
		}
	}
	
	private class DeviceRenameAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
				new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.R, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
			};
		}
		
		@Override
		public void action(KeyboardListener t) {
			if(selectedWrappers.size() > 0) {
				new DeviceNameEditorStage(selectedWrappers.get(selectedWrappers.size()-1), SynthEditorPane.this);
			}
		}
	}
	
	private class DeviceBypassAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
				new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.B, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
			};
		}
		
		@Override
		public void action(KeyboardListener t) {
			if(getLatestSelectedWrapper() != null) {
				getLatestSelectedWrapper().setBypassed(!getLatestSelectedWrapper().isBypassed());
			}
		}
	}
	
	private class DeviceEditAction implements KeyboardAction<KeyboardListener> {

		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.E, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
			};
		}

		@Override
		public void action(KeyboardListener t) {
			if(getLatestSelectedWrapper() != null) {
				openDeviceParametersEditor(getLatestSelectedWrapper());
			}
		}
	}
	
	private class DeviceSearchAction implements KeyboardAction<KeyboardListener> {

		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.SPACE, InputState.NO, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			setTempDevice(null);
			if(!deviceSearchPopup.isShowing()) {
				deviceSearchPopup = new DeviceSearchPopupFx(SynthEditorPane.this);
				deviceSearchPopup.show(SynthEditorPane.this, mouseListener.currentEvent.getScreenX(), mouseListener.currentEvent.getScreenY());
			}
		}
	}
	
	private class UndoAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.Z, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			AbstractEditorHistoryEvent abstractHistoryEvent = history.getLastHistoryEvent();
			if(abstractHistoryEvent == null) {
				return;
			}
			
			if(abstractHistoryEvent instanceof EditorHistoryEventModuleAddRemove) {
				EditorHistoryEventModuleAddRemove historyEvent = (EditorHistoryEventModuleAddRemove) abstractHistoryEvent;
				if(historyEvent.getAddedSubjects() != null) {
					for(Object subject: historyEvent.getAddedSubjects()) {
						if(subject instanceof ModuleWrapper) {
							removeDevice((ModuleWrapper) subject, false, false);
						}
						else if(subject instanceof ModuleConnector) {
							removeDeviceConnector((ModuleConnector) subject, false, false);
						}
					}
				}
				
				if(historyEvent.getRemovedSubjects() != null) {
					for(Object subject: historyEvent.getRemovedSubjects()) {
						if(subject instanceof ModuleWrapper) {
							addDevice((ModuleWrapper) subject, false, false);
						}
						else if(subject instanceof ModuleConnector) {
							addDeviceConnector((ModuleConnector) subject, false, false);
						}
					}
				}
			}
			else if(abstractHistoryEvent instanceof EditorHistoryEventModuleMove) {
				EditorHistoryEventModuleMove historyEvent = (EditorHistoryEventModuleMove) abstractHistoryEvent;
				for(int i = 0; i < historyEvent.getMoveSubjects().length; i++) {
					ModuleWrapper movedDevice = (ModuleWrapper) historyEvent.getMoveSubjects()[i];
					Coordinate oldCoorinate = historyEvent.getOldCoordinates()[i];
					
					movedDevice.posX = oldCoorinate.x;
					movedDevice.posY = oldCoorinate.y;
				}
			}
			else if(abstractHistoryEvent instanceof EditorHistoryEventModuleEdit) {
				EditorHistoryEventModuleEdit historyEvent = (EditorHistoryEventModuleEdit) abstractHistoryEvent;
				historyEvent.getSubject().module.fromElement(historyEvent.getOldParameterElement());
			}
			
			registerUnsavedChangePerformed();
			repaint();
		}
	}
	
	private class RedoAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.Y, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.NO, InputState.NO, InputState.NO, InputState.NO)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			AbstractEditorHistoryEvent abstractHistoryEvent = history.getNextHistoryEvent();
			if(abstractHistoryEvent == null) {
				return;
			}
			
			if(abstractHistoryEvent instanceof EditorHistoryEventModuleAddRemove) {
				EditorHistoryEventModuleAddRemove historyEvent = (EditorHistoryEventModuleAddRemove) abstractHistoryEvent;
				if(historyEvent.getAddedSubjects() != null) {
					for(Object subject: historyEvent.getAddedSubjects()) {
						if(subject instanceof ModuleWrapper) {
							addDevice((ModuleWrapper) subject, false, false);
						}
						else if(subject instanceof ModuleConnector) {
							addDeviceConnector((ModuleConnector) subject, false, false);
						}
					}
				}
				
				if(historyEvent.getRemovedSubjects() != null) {
					for(Object subject: historyEvent.getRemovedSubjects()) {
						if(subject instanceof ModuleWrapper) {
							removeDevice((ModuleWrapper) subject, false, false);
						}
						else if(subject instanceof ModuleConnector) {
							removeDeviceConnector((ModuleConnector) subject, false, false);
						}
					}
				}
			}
			else if(abstractHistoryEvent instanceof EditorHistoryEventModuleMove) {
				EditorHistoryEventModuleMove historyEvent = (EditorHistoryEventModuleMove) abstractHistoryEvent;
				for(int i = 0; i < historyEvent.getMoveSubjects().length; i++) {
					ModuleWrapper movedDevice = (ModuleWrapper) historyEvent.getMoveSubjects()[i];
					Coordinate newCoorinate = historyEvent.getNewCoordinates()[i];
					
					movedDevice.posX = newCoorinate.x;
					movedDevice.posY = newCoorinate.y;
				}
			}
			else if(abstractHistoryEvent instanceof EditorHistoryEventModuleEdit) {
				EditorHistoryEventModuleEdit historyEvent = (EditorHistoryEventModuleEdit) abstractHistoryEvent;
				historyEvent.getSubject().module.fromElement(historyEvent.getNewParameterElement());
			}
			
			registerUnsavedChangePerformed();
			repaint();
		}
	}
	
	private class EscapeAction implements KeyboardAction<KeyboardListener> {

		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.ESCAPE, InputState.NO, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			setTempDevice(null);
			setTempSynth(null);
			tempConnector = null;
			repaint();
		}
		
	}
	
	private class CutAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.X, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			//Find the selected connectors
			ArrayList<ModuleConnector> selectedConnectors = new ArrayList<ModuleConnector>();
			for(ModuleWrapper wrapper: selectedWrappers) {
				for(ModuleConnector connector: synth.getConnectorsByWrapper(wrapper)) {
					if(!selectedConnectors.contains(connector) && selectedWrappers.contains(connector.inputWrapper) && selectedWrappers.contains(connector.outputWrapper)) {
						selectedConnectors.add(connector);
					}
				}
			}
			
			//Instance a tempsynth
			TempSynth temp = new TempSynth(getSynth().getName() + "_clipboard", selectedWrappers, selectedConnectors, getSynth());
			
			final Clipboard cliphboard = Clipboard.getSystemClipboard();
			final ClipboardContent content = new ClipboardContent();
			content.putString(temp.toElement().toDocumentformat());
			cliphboard.setContent(content);
			
			removeDevices(selectedWrappers.toArray(new ModuleWrapper[0]), true, true);
			clearSelectedDevices();
		}
		
	}
	
	private class CopyAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.C, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			//Find the selected connectors
			ArrayList<ModuleConnector> selectedConnectors = new ArrayList<ModuleConnector>();
			for(ModuleWrapper wrapper: selectedWrappers) {
				for(ModuleConnector connector: synth.getConnectorsByWrapper(wrapper)) {
					if(!selectedConnectors.contains(connector) && selectedWrappers.contains(connector.inputWrapper) && selectedWrappers.contains(connector.outputWrapper)) {
						selectedConnectors.add(connector);
					}
				}
			}
			
			//Instance a tempsynth
			TempSynth temp = new TempSynth(getSynth().getName() + "_clipboard", selectedWrappers, selectedConnectors, getSynth());
			
			final Clipboard cliphboard = Clipboard.getSystemClipboard();
			final ClipboardContent content = new ClipboardContent();
			content.putString(temp.toElement().toDocumentformat());
			cliphboard.setContent(content);
		}
		
	}
	
	private class PasteAction implements KeyboardAction<KeyboardListener> {
		
		@Override
		public EditorKeyboardInputFilter[] getActionFilters() {
			return new EditorKeyboardInputFilter[] {
					new EditorKeyboardInputFilter(KeyEvent.KEY_RELEASED, KeyCode.V, InputState.YES, InputState.NO, InputState.NO, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED, InputState.UNUSED)
					};
		}

		@Override
		public void action(KeyboardListener t) {
			final Clipboard cliphboard = Clipboard.getSystemClipboard();
			String tempSynthString = cliphboard.getString();
			if(tempSynthString == null) {
				return;
			}
			else if(!tempSynthString.startsWith("<synth>")) {
				return;
			}
			
			TempSynth tmp = new TempSynth(new Element(tempSynthString), getSynth());
			tmp.reinstance(getSynth());
			setTempSynth(tmp);
		}
		
	}
}