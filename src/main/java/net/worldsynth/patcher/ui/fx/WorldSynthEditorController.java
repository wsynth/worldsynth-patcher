/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.patcher.ui.fx.extentseditor.ExtentsEditor;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorPane;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.ProjectReader;
import net.worldsynth.synth.io.ProjectWriter;

public class WorldSynthEditorController implements Initializable {
	
	private final Logger LOGGER = WorldSynthPatcher.LOGGER;
	
	public static WorldSynthEditorController instance;
	
	@FXML public SynthTreeView synthTree;
	public SynthEditorPane currentSynthEditor;
	public ModuleWrapper currentPreviewDevice;
	
	@FXML public TabPane editorTabPane;
	@FXML public PreviewPane previewPane;
	@FXML public ExtentsEditor extentsEditor;
	@FXML public Infobar infobar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		instance = this;
		
		editorTabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
				if(newValue.getContent() instanceof SynthEditorPane) {
					currentSynthEditor = (SynthEditorPane) newValue.getContent();
					//Update tree view
					synthTree.setSynthEditor(currentSynthEditor);
					//Update extents
					currentSynthEditor.getSynth().getExtentManager();
				}
			}
		});
		
		Synth defaultSynth = new Synth("UnnamedSynth");
		openSynthEditor(defaultSynth, null);
		extentsEditor.setExtentManager(defaultSynth.getExtentManager());
		previewPane.setExtentManager(defaultSynth.getExtentManager());
	}
	
	@FXML protected void newSynth() {
		openSynthEditor(new Synth("Unnamed synth"), null);
	}
	
	@FXML protected void openSynth() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));
		
		File synthFile = fileChooser.showOpenDialog(WorldSynthPatcher.primaryStage);
		
		LOGGER.info(synthFile);
		if(synthFile != null) {
			Synth openedSynth = ProjectReader.readSynthFromFile(synthFile);
			openSynthEditor(openedSynth, synthFile);
		}
	}
	
	public void openSynthEditor(Synth synth, File synthFile) {
		for(Tab t: editorTabPane.getTabs()) {
			if(t.getContent() instanceof SynthEditorPane) {
				if(((SynthEditorPane) t.getContent()).getSynth() == synth) {
					editorTabPane.getSelectionModel().select(t);
					return;
				}
			}
		}
		Tab synthEditorTab = new Tab(synth.getName());
		SynthEditorPane newSynthEditor = new SynthEditorPane(synth, synthFile, synthEditorTab);
		currentSynthEditor = newSynthEditor;
		synthEditorTab.setContent(newSynthEditor);
		editorTabPane.getTabs().add(synthEditorTab);
		editorTabPane.getSelectionModel().select(synthEditorTab);
		synthTree.setRoot(newSynthEditor.getDeviceTree());
		
		synthEditorTab.setOnCloseRequest(e -> {
			if(((Tab) e.getSource()).getContent() == currentSynthEditor) {
				currentSynthEditor = null;
				synthTree.clearTreeView();
			}
		});
	}
	
	public void updatePreview() {
		updatePreview(true);
	}
	
	public void updatePreview(boolean clearExtentsEditorPreview) {
		previewPane.updatePreview();
		extentsEditor.worldPreview.updatePreview(clearExtentsEditorPreview);
	}
	
	public void updatePeview(Synth synth, ModuleWrapper device) {
		currentPreviewDevice = device;
		if(synth.getExtentManager() != extentsEditor.getCurrentExtentManager()) {
			extentsEditor.setExtentManager(synth.getExtentManager());
			previewPane.setExtentManager(synth.getExtentManager());
		}

		extentsEditor.worldPreview.updatePreview(synth, device);
		previewPane.updatePreview(synth, device);
	}
	
	@FXML protected void saveSynth() {
		File file = currentSynthEditor.getSaveFile();
		if(file == null) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open WorldSynth project");
			fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));
			
			file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		}
		if(file == null) {
			return;
		}
		
		String synthName = file.getName();
		synthName = synthName.substring(0, synthName.lastIndexOf(".wsynth"));
		
		renameSynthInEditor(currentSynthEditor.getSynth(), synthName);
		currentSynthEditor.setSaveFile(file);
		
		ProjectWriter.writeSynthToFile(currentSynthEditor.getSynth(), file);
		currentSynthEditor.registerSavePerformed();
	}
	
	@FXML protected void saveSynthAs() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));
		
		File file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		if(file == null) {
			return;
		}
		
		String synthName = file.getName();
		synthName = synthName.substring(0, synthName.lastIndexOf(".wsynth"));
		
		renameSynthInEditor(currentSynthEditor.getSynth(), synthName);
		currentSynthEditor.setSaveFile(file);
		
		ProjectWriter.writeSynthToFile(currentSynthEditor.getSynth(), file);
		currentSynthEditor.registerSavePerformed();
	}
	
	private void renameSynthInEditor(Synth synth, String name) {
		for(Tab t: editorTabPane.getTabs()) {
			if(t.getContent() instanceof SynthEditorPane) {
				SynthEditorPane synthEditor = (SynthEditorPane) t.getContent();
				if(synthEditor.getSynth() == synth) {
					t.setText(name);
					synth.setName(name);
					return;
				}
			}
		}
	}
	
	@FXML protected void about() {
		new AboutStage();
	}
	
	@FXML protected void issuetracker() {
		WorldSynthPatcher.instance.getHostServices().showDocument("https://www.issuetracker.worldsynth.net");
	}
	
	@FXML protected void reddit() {
		WorldSynthPatcher.instance.getHostServices().showDocument("https://www.reddit.worldsynth.net");
	}
	
	@FXML protected void discord() {
		WorldSynthPatcher.instance.getHostServices().showDocument("https://www.discord.worldsynth.net");
	}
}
