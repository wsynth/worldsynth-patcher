/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import net.worldsynth.module.ModuleMacro;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.WorldSynthEditorController;
import net.worldsynth.standalone.ui.stage.DeviceNameEditorStage;
import net.worldsynth.standalone.ui.stage.ModuleParametersStage;

public class ModuleEditContextMenu extends ContextMenu {
	
	public ModuleEditContextMenu(ModuleWrapper device, SynthEditorPane synthEditor) {
		
		synthEditor.setOnMousePressed(e -> {
			hide();
		});
		
		//PARAMETERS//
		MenuItem openModuleUI = new MenuItem("Parameters");
		openModuleUI.setOnAction(e -> {
			new ModuleParametersStage(device);
		});
		getItems().add(openModuleUI);
		
		//RENAME//
		MenuItem openDeviceCustomName = new MenuItem("Rename");
		openDeviceCustomName.setOnAction(e -> {
			new DeviceNameEditorStage(device, synthEditor);
		});
		getItems().add(openDeviceCustomName);
		
		//BYPASS//
		MenuItem bypasDevice = new MenuItem("Bypass");
		bypasDevice.setDisable(!device.isBypassable());
		bypasDevice.setOnAction(e -> {
			device.setBypassed(!device.isBypassed());
		});
		getItems().add(bypasDevice);
		
		//DELETE//
		MenuItem deleteDevice = new MenuItem("Delete");
		deleteDevice.setOnAction(e -> {
			synthEditor.removeDevice(device, true, true);
		});
		getItems().add(deleteDevice);
		
		if(device.module instanceof ModuleMacro) {
			MenuItem openMacro = new MenuItem("OpenMacro");
			openMacro.setOnAction(e -> {
				WorldSynthEditorController.instance.openSynthEditor(((ModuleMacro)device.module).getMacroSynth(), null); 
			});
			getItems().add(openMacro);
		}
	}
}
