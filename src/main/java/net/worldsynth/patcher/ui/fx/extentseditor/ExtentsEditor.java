/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.extentseditor;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.patcher.ui.fx.WorldSynthEditorController;

public class ExtentsEditor extends BorderPane {
	
	private WorldExtentManager currentExtentsManager = null;
	
	public WorldPreviewPane worldPreview = new WorldPreviewPane();
	
	private TextField valueFieldName = new TextField("");
	private WorldSynthCustomDoubleNumberField valueFieldX = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldZ = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldWidth = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldLength = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private CheckBox checkBoxLockSizeRatio = new CheckBox("Lock size ratio");
	private ListView<WorldExtent> extentList = new ListView<WorldExtent>();
	private int newExtentCount = 0;
	
	public ExtentsEditor() {
		//Name field events
		valueFieldName.setOnAction(e -> {
			WorldExtent currentExtent = currentExtentsManager.getCurrentWorldExtent();
			currentExtent.setName(valueFieldName.getText());
			currentExtentsManager.setCurrentWorldExtent(null);
			currentExtentsManager.setCurrentWorldExtent(currentExtent);
			
			extentList.refresh();
		});
		
		valueFieldName.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if(newValue == false) {
				//Reset the name in the name field if the field losses focus without the user applying the new name by hitting enter
				valueFieldName.setText(currentExtentsManager.getCurrentWorldExtent().getName());
			}
		});
		
		//Create button for adding new extent to the list, and set the action handler for it to add the new extent.
		Button addExtentButton = new Button("Add extent");
		addExtentButton.setOnAction(e -> {
			WorldExtent newExtent = new WorldExtent("Unnamed extent " + newExtentCount++, -128, -128, 256);
			currentExtentsManager.addWorldExtent(newExtent);
			currentExtentsManager.setCurrentWorldExtent(newExtent);
		});
		
		//Create button for removing new extent to the list, and set the action handler for it to remove the current extent.
		Button removeExtentButton = new Button("Remove extent");
		removeExtentButton.setOnAction(e -> {
			currentExtentsManager.removeWorldExtent(currentExtentsManager.getCurrentWorldExtent());
		});
		
		//All editable fields are disabled until an extent has been bound to them trough bindNewExtent(WorldExtent, WorldExtent)
		valueFieldName.setDisable(true);
		valueFieldX.setDisable(true);
		valueFieldZ.setDisable(true);
		checkBoxLockSizeRatio.setDisable(true);
		valueFieldWidth.setDisable(true);
		valueFieldLength.setDisable(true);
		
		//Build the UI, adding all components of it.
		GridPane extentsPane = new GridPane();
		extentsPane.setPadding(new Insets(10.0));
		extentsPane.setHgap(10.0);
		extentsPane.setVgap(5.0);
		
		Label extentLabel = new Label("Current extent:");
		GridPane.setColumnSpan(extentLabel, 2);
		extentsPane.add(extentLabel, 0, 0);
		GridPane.setColumnSpan(valueFieldName, 2);
		extentsPane.add(valueFieldName, 0, 1);
		
		Label extentXLabel = new Label("X:");
		extentsPane.add(extentXLabel, 0, 2);
		extentsPane.add(valueFieldX, 1, 2);
		
		Label extentZLabel = new Label("Z:");
		extentsPane.add(extentZLabel, 0, 3);
		extentsPane.add(valueFieldZ, 1, 3);
		
		GridPane.setColumnSpan(checkBoxLockSizeRatio, 2);
		extentsPane.add(checkBoxLockSizeRatio, 0, 4);
		
		Label extentWidthLabel = new Label("Width:");
		extentsPane.add(extentWidthLabel, 0, 5);
		extentsPane.add(valueFieldWidth, 1, 5);
		
		Label extentLengthLabel = new Label("Length:");
		extentsPane.add(extentLengthLabel, 0, 6);
		extentsPane.add(valueFieldLength, 1, 6);
		
		GridPane.setColumnSpan(extentList, 2);
		extentsPane.add(extentList, 0, 7);
		
		extentsPane.add(addExtentButton, 0, 8);
		extentsPane.add(removeExtentButton, 1, 8);
		
		setLeft(extentsPane);
		setCenter(worldPreview);
	}
	
	/**
	 * This listener is used to listen for changes to the current extent and update the preview window in top left corner if the extent changes in position or size.<br>
	 * The listener is registered to the appropriate properties inside {@link #bindNewExtent(WorldExtent, WorldExtent) bindNewExtent}
	 */
	private ChangeListener<Number> extentRegionChangeListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
		if(!currentExtentsManager.getCurrentWorldExtent().blockPreviewUpdateEvent()) {
			WorldSynthEditorController.instance.updatePreview(false);
		}
	};
	
	/**
	 * Changes the extent binded to the UI controls for extent configuring.<br>
	 * Unbinds the old extent if there is one from the UI controlls for it's properties and binds the new in it's place if there is one.
	 */
	private void bindNewExtent(WorldExtent oldExtent, WorldExtent newExtent) {
		if(oldExtent != null) {
			oldExtent.xProperty().removeListener(extentRegionChangeListener);
			oldExtent.yProperty().removeListener(extentRegionChangeListener);
			oldExtent.zProperty().removeListener(extentRegionChangeListener);
			oldExtent.widthProperty().removeListener(extentRegionChangeListener);
			oldExtent.heightProperty().removeListener(extentRegionChangeListener);
			oldExtent.lengthProperty().removeListener(extentRegionChangeListener);
			
			Bindings.unbindBidirectional(oldExtent.xProperty(), valueFieldX.valueProperty());
			Bindings.unbindBidirectional(oldExtent.zProperty(), valueFieldZ.valueProperty());
			Bindings.unbindBidirectional(oldExtent.sizeRatioLockProperty(), checkBoxLockSizeRatio.selectedProperty());
			Bindings.unbindBidirectional(oldExtent.widthProperty(), valueFieldWidth.valueProperty());
			Bindings.unbindBidirectional(oldExtent.lengthProperty(), valueFieldLength.valueProperty());
		}
		
		if(newExtent == null) {
			valueFieldName.setDisable(true);
			valueFieldX.setDisable(true);
			valueFieldZ.setDisable(true);
			checkBoxLockSizeRatio.setDisable(true);
			valueFieldWidth.setDisable(true);
			valueFieldLength.setDisable(true);
			
			valueFieldName.setText("");
			valueFieldX.setValue(Double.NaN);
			valueFieldZ.setValue(Double.NaN);
			valueFieldWidth.setValue(Double.NaN);
			valueFieldLength.setValue(Double.NaN);
		}
		else {
			valueFieldName.setDisable(false);
			valueFieldX.setDisable(false);
			valueFieldZ.setDisable(false);
			checkBoxLockSizeRatio.setDisable(false);
			valueFieldWidth.setDisable(false);
			valueFieldLength.setDisable(false);
			
			valueFieldName.setText(newExtent.getName());
			Bindings.bindBidirectional(valueFieldX.valueProperty(), newExtent.xProperty());
			Bindings.bindBidirectional(valueFieldZ.valueProperty(), newExtent.zProperty());
			Bindings.bindBidirectional(checkBoxLockSizeRatio.selectedProperty(), newExtent.sizeRatioLockProperty());
			Bindings.bindBidirectional(valueFieldWidth.valueProperty(), newExtent.widthProperty());
			Bindings.bindBidirectional(valueFieldLength.valueProperty(), newExtent.lengthProperty());
			
			newExtent.xProperty().addListener(extentRegionChangeListener);
			newExtent.yProperty().addListener(extentRegionChangeListener);
			newExtent.zProperty().addListener(extentRegionChangeListener);
			newExtent.widthProperty().addListener(extentRegionChangeListener);
			newExtent.heightProperty().addListener(extentRegionChangeListener);
			newExtent.lengthProperty().addListener(extentRegionChangeListener);
		}
	}
	
	/**
	 * This listener listens for change in the current worldextent in the extents manager.<br>
	 * The listener is registered to the appropriate properties inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentManagerSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		extentList.getSelectionModel().select(newValue);
		bindNewExtent(oldValue, newValue);
	};
	
	/**
	 * This listener listens for change in the selected worldextent in the extents list.<br>
	 * The listener is added and removed to the appropriate property inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentListSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		if(currentExtentsManager != null) {
			currentExtentsManager.setCurrentWorldExtent(newValue);
		}
	};
	
	public void setExtentManager(WorldExtentManager manager) {
		extentList.getSelectionModel().selectedItemProperty().removeListener(extentListSelectionChangeListener);
		WorldExtentManager oldExtentManger = currentExtentsManager;
		if(currentExtentsManager != null) {
			currentExtentsManager.currentWorldExtentProperty().removeListener(extentManagerSelectionChangeListener);
			bindNewExtent(oldExtentManger.getCurrentWorldExtent(), null);
		}
		currentExtentsManager = manager;
		if(currentExtentsManager == null) {
			extentList.setItems(FXCollections.emptyObservableList());
			extentList.getSelectionModel().clearSelection();
		}
		else {
			currentExtentsManager.currentWorldExtentProperty().addListener(extentManagerSelectionChangeListener);
			extentList.setItems(manager.getObservableExtentsList());
			extentList.getSelectionModel().select(manager.getCurrentWorldExtent());
			bindNewExtent(null, manager.getCurrentWorldExtent());
		}
		extentList.getSelectionModel().selectedItemProperty().addListener(extentListSelectionChangeListener);
	}
	
	public WorldExtentManager getCurrentExtentManager() {
		return currentExtentsManager;
	}
}
