/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.AutoCompletionBinding.ISuggestionRequest;
import org.controlsfx.control.textfield.TextFields;

import impl.org.controlsfx.autocompletion.SuggestionProvider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Popup;
import javafx.util.Callback;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.statistics.WorldSynthStatTracker;

public class DeviceSearchPopupFx extends Popup {
	
	private SynthEditorPane synthEditor;
	
	public DeviceSearchPopupFx(SynthEditorPane synthEditor) {
		this.synthEditor = synthEditor;
		
		setAutoHide(true);
		
		TextField searchField = new TextField();
		searchField.setPrefWidth(400.0);
		
		ArrayList<ModuleEntry> moduleEntries = WorldSynthCore.moduleRegister.getRegisteredModuleEntries();
		Collections.sort(moduleEntries, new ModuleEntrySort());
		AutoCompletionBinding<ModuleEntry> binding = TextFields.bindAutoCompletion(searchField, new SuggestionProviderModuleEntry(moduleEntries));
		binding.setPrefWidth(400.0);
		binding.setDelay(0);
		
		binding.setOnAutoCompleted(e -> {
			hide();
			ModuleEntry me = e.getCompletion();
			addModuleToSynth(me);
		});
		
		addEventHandler(KeyEvent.ANY, e -> {
			if(e.getCode() == KeyCode.ESCAPE) {
				hide();
			}
		});
		
		getContent().add(searchField);
	}
	
	private void addModuleToSynth(ModuleEntry moduleEntry) {
		try {
			WorldSynthStatTracker.incrementModuleUse(moduleEntry.getModuleClass());
			synthEditor.setTempDevice(new ModuleWrapper(moduleEntry, synthEditor.getSynth(), 0, 0));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private class ModuleEntrySort implements Comparator<ModuleEntry> {
		@Override
		public int compare(ModuleEntry entry1, ModuleEntry entry2) {
			int entry1Usage = WorldSynthStatTracker.getModuleUseCount(entry1.getModuleClassString());
			int entry2Usage = WorldSynthStatTracker.getModuleUseCount(entry2.getModuleClassString());
			return entry2Usage - entry1Usage;
		}
	}
	
	
	/**
     * This is a simple string based suggestion provider.
     * All generic suggestions T are turned into strings for processing.
     * 
     */
    private static class SuggestionProviderModuleEntry extends SuggestionProvider<ModuleEntry> {

        private Callback<ModuleEntry, String> stringConverter = new Callback<ModuleEntry, String>() {
            @Override
            public String call(ModuleEntry obj) {
                return obj != null ? obj.toString() : "";
            }
        };

        private final Comparator<ModuleEntry> stringComparator = new Comparator<ModuleEntry>() {
            @Override
            public int compare(ModuleEntry o1, ModuleEntry o2) {
            	return 0;
            }
        };

        /**
         * Create a new SuggestionProviderModuleEntry
         * @param possibleSugestions
         */
        public SuggestionProviderModuleEntry(Collection<ModuleEntry> possibleSuggestions) {
        	addPossibleSuggestions(possibleSuggestions);
        }

        /**{@inheritDoc}*/
        @Override
        protected Comparator<ModuleEntry> getComparator() {
            return stringComparator;
        }

        /**{@inheritDoc}*/
        @Override
        protected boolean isMatch(ModuleEntry suggestion, ISuggestionRequest request) {
            String[] userTextLower = request.getUserText().toLowerCase().split(" ");
			String suggestionStr = stringConverter.call(suggestion).toLowerCase();
			return stringContains(suggestionStr, userTextLower);
        }
        
        private boolean stringContains(String subject, String[] subStrings) {
			for(String s: subStrings) {
				if(!subject.toLowerCase().contains(s.toLowerCase())) {
					return false;
				}
			}
			return true;
		}
    }
}
