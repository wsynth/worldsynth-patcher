/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.extentseditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;
import net.worldsynth.common.math.Vector2d;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtent.ExtentCorner;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.modulewrapper.ModuleWrapperIO;
import net.worldsynth.patcher.build.BuildTask;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.patcher.ui.navcanvas.Pixel;
import net.worldsynth.synth.Synth;
import net.worldsynth.util.event.build.BuildStatusListener;

public class WorldPreviewPane extends Pane implements NavigationalCanvas {
	private final Canvas canvas = new Canvas();
	
	private PreviewChunkBuildTask currentBuildTask = null;
	private Synth currentPreviewSynth = null;
	private ModuleWrapper currentPreviewWrapper = null;
	
	private ArrayList<PreviewChunk> chunksToBuild = new ArrayList<PreviewChunk>();
	private ArrayList<PreviewChunk> builtChunks = new ArrayList<PreviewChunk>();
	
	private double centerCoordX = 0.0;
	private double centerCoordY = 0.0;
	private double zoom = 1.0;
	
	private double lastMouseX = 0;
	private double lastMouseY = 0;
	
	/**Current extent mouse is hovering over*/
	private WorldExtent mouseOverExtent = null;
	private ExtentCorner mouseOverCorner = null;
	/**Used as a temporary extent for rendering while adjusting an extent*/
	private WorldExtent adjustmentExtent = null;
	
	public WorldPreviewPane() {
		getChildren().add(canvas);
		
		setOnMouseExited(e -> {
			if(mouseOverExtent != null && adjustmentExtent == null) {
				mouseOverExtent = null;
				mouseOverCorner = null;
				updatePreview(false);
			}
		});
		
		setOnMouseMoved(e -> {
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			WorldExtent lastMouseOverExtent = mouseOverExtent;
			ExtentCorner lastMouseOverCorner = mouseOverCorner;
			if(adjustmentExtent == null && currentPreviewSynth != null) {
				hoverExtentFromCoordinate(e.getX(), e.getY());
				if(mouseOverExtent != lastMouseOverExtent || mouseOverCorner != lastMouseOverCorner) {
					updatePreview(false);
				}
			}
		});
		
		setOnMouseDragged(e -> {
			double diffX = lastMouseX - e.getX();
			double diffY = lastMouseY - e.getY();
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			if(e.isSecondaryButtonDown()) {
				centerCoordX += diffX / zoom;
				centerCoordY += diffY / zoom;
				
				updatePreview(false);
			}
			else if(adjustmentExtent != null) {
				boolean lockSizeRatio = adjustmentExtent.getSizeRatioLock();
				if(mouseOverCorner == null) {
					adjustmentExtent.setX(adjustmentExtent.getX() - diffX / zoom);
					adjustmentExtent.setZ(adjustmentExtent.getZ() - diffY / zoom);
				}
				else if(mouseOverCorner == ExtentCorner.NW) {
					if(lockSizeRatio) {
						//vectorProjection
						Vector2d ratioVector = Vector2d.normalize(new Vector2d(adjustmentExtent.getSizeRatio(), 1.0));
						Vector2d adjustementVecor = Vector2d.multiply(ratioVector, Vector2d.dotProduct(new Vector2d(diffX, diffY), ratioVector));
						
						adjustmentExtent.setX(adjustmentExtent.getX() - adjustementVecor.getX() / zoom);
						adjustmentExtent.setZ(adjustmentExtent.getZ() - adjustementVecor.getY() / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() + adjustementVecor.getX() / zoom);
					}
					else {
						adjustmentExtent.setX(adjustmentExtent.getX() - diffX / zoom);
						adjustmentExtent.setZ(adjustmentExtent.getZ() - diffY / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() + diffX / zoom);
						adjustmentExtent.setLength(adjustmentExtent.getLength() + diffY / zoom);
					}
				}
				else if(mouseOverCorner == ExtentCorner.NE) {
					if(lockSizeRatio) {
						//vectorProjection
						Vector2d ratioVector = Vector2d.normalize(new Vector2d(adjustmentExtent.getSizeRatio(), -1.0));
						Vector2d adjustementVecor = Vector2d.multiply(ratioVector, Vector2d.dotProduct(new Vector2d(diffX, diffY), ratioVector));
						
						adjustmentExtent.setZ(adjustmentExtent.getZ() - adjustementVecor.getY() / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() - adjustementVecor.getX() / zoom);
					}
					else {
						adjustmentExtent.setZ(adjustmentExtent.getZ() - diffY / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() - diffX / zoom);
						adjustmentExtent.setLength(adjustmentExtent.getLength() + diffY / zoom);
					}
				}
				else if(mouseOverCorner == ExtentCorner.SW) {
					if(lockSizeRatio) {
						//vectorProjection
						Vector2d ratioVector = Vector2d.normalize(new Vector2d(adjustmentExtent.getSizeRatio(), -1.0));
						Vector2d adjustementVecor = Vector2d.multiply(ratioVector, Vector2d.dotProduct(new Vector2d(diffX, diffY), ratioVector));
						
						adjustmentExtent.setX(adjustmentExtent.getX() - adjustementVecor.getX() / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() + adjustementVecor.getX() / zoom);
					}
					else {
						adjustmentExtent.setX(adjustmentExtent.getX() - diffX / zoom);
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() + diffX / zoom);
						adjustmentExtent.setLength(adjustmentExtent.getLength() - diffY / zoom);
					}
				}
				else if(mouseOverCorner == ExtentCorner.SE) {
					if(lockSizeRatio) {
						//vectorProjection
						Vector2d ratioVector = Vector2d.normalize(new Vector2d(adjustmentExtent.getSizeRatio(), 1.0));
						Vector2d adjustementVecor = Vector2d.multiply(ratioVector, Vector2d.dotProduct(new Vector2d(diffX, diffY), ratioVector));
						
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() - adjustementVecor.getX() / zoom);
					}
					else {
						adjustmentExtent.setWidth(adjustmentExtent.getWidth() - diffX / zoom);
						adjustmentExtent.setLength(adjustmentExtent.getLength() - diffY / zoom);
					}
				}
				
				updatePreview(false);
			}
			else if(e.isPrimaryButtonDown() && mouseOverExtent != null) {
				adjustmentExtent = mouseOverExtent.clone();
				if(mouseOverCorner == null) {
					adjustmentExtent.setX(adjustmentExtent.getX() - diffX / zoom);
					adjustmentExtent.setZ(adjustmentExtent.getZ() - diffY / zoom);
				}
				
				updatePreview(false);
			}
		});
		
		setOnMouseReleased(e -> {
			if(e.getButton() == MouseButton.PRIMARY && adjustmentExtent != null) {
				adjustmentExtent.setX(Math.floor(adjustmentExtent.getX()));
				adjustmentExtent.setZ(Math.floor(adjustmentExtent.getZ()));
				adjustmentExtent.setWidth(Math.floor(adjustmentExtent.getWidth()));
				adjustmentExtent.setLength(Math.floor(adjustmentExtent.getLength()));
				mouseOverExtent.cloneValuesFrom(adjustmentExtent);
				adjustmentExtent = null;
				
				updatePreview(false);
			}
		});
		
		setOnScroll(e -> {
			//Do not zoom on direct input
			if(e.isDirect()) return;
			
			double maxZoom = 1.0;
			double minZoom = 0.05;
			
			double lastZoom = zoom;
			zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
			if(zoom < minZoom) zoom = minZoom;
			else if(zoom > maxZoom) zoom = maxZoom;
			
			if(lastZoom != zoom) {
				if(adjustmentExtent == null && currentPreviewSynth != null) {
					hoverExtentFromCoordinate(e.getX(), e.getY());
				}
				updatePreview(true);
			}
		});
		
		setOnMouseClicked(e -> {
			if(adjustmentExtent != null && e.getButton() == MouseButton.SECONDARY && e.isStillSincePress()) {
				adjustmentExtent = null;
				updatePreview(false);
			}
			else if(e.getClickCount() == 2 && mouseOverExtent != null) {
				currentPreviewSynth.getExtentManager().setCurrentWorldExtent(mouseOverExtent);
			}
		});
		
		repaint();
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
        
        updatePreview(false);
    }
	
	@Override
	public double getCenterCoordinateX() {
		return centerCoordX;
	}

	@Override
	public double getCenterCoordinateY() {
		return centerCoordY;
	}

	@Override
	public double getZoom() {
		return zoom;
	}
	
	//Paint stuff
	private void repaint() {
		paint();
	}
	
	
	private void paint() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(Color.web("#303030"));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		for(PreviewChunk c: builtChunks) {
			if(chunkInView(c)) {
				paintChunk(c, g);
			}
		}
		
		//Paint the grid
		paintGrid(g);
		
		//Paint worldextents
		if(currentPreviewSynth != null) {
			for(WorldExtent extent: currentPreviewSynth.getExtentManager().getObservableExtentsList()) {
				if(extent == mouseOverExtent && adjustmentExtent != null) {
					paintExtent(adjustmentExtent, g);
				}
				else {
					paintExtent(extent, g);
				}
			}
		}
	}
	
	
	private void paintExtent(WorldExtent worldExtent, GraphicsContext g) {
		Pixel corner = new Pixel(new Coordinate(worldExtent.getX(), worldExtent.getZ()), this);
		
		g.setStroke(Color.BLACK);
		g.setLineWidth(2.0);
		if(worldExtent == currentPreviewSynth.getExtentManager().getCurrentWorldExtent()) {
			g.setStroke(Color.RED);
		}
		else if(worldExtent == adjustmentExtent) {
			g.setStroke(Color.WHITE);
		}
		g.strokeRect(corner.x, corner.y, worldExtent.getWidth()*zoom, worldExtent.getLength()*zoom);
		if(worldExtent == mouseOverExtent || worldExtent == adjustmentExtent) {
			g.setFill(Color.color(1.0, 1.0, 1.0, 0.1));
			g.fillRect(corner.x, corner.y, worldExtent.getWidth()*zoom, worldExtent.getLength()*zoom);
			
			g.setStroke(Color.color(1.0, 1.0, 1.0, 0.5));
			if(mouseOverCorner == ExtentCorner.NW) {
				g.strokeLine(corner.x - 10.0, corner.y - 10.0, corner.x + 10.0, corner.y + 10.0);
				g.strokeLine(corner.x - 10.0, corner.y + 10.0, corner.x + 10.0, corner.y - 10.0);
			}
			else if(mouseOverCorner == ExtentCorner.NE) {
				g.strokeLine(corner.x + worldExtent.getWidth()*zoom - 10.0, corner.y - 10.0, corner.x + worldExtent.getWidth()*zoom + 10.0, corner.y + 10.0);
				g.strokeLine(corner.x + worldExtent.getWidth()*zoom - 10.0, corner.y + 10.0, corner.x + worldExtent.getWidth()*zoom + 10.0, corner.y - 10.0);
			}
			else if(mouseOverCorner == ExtentCorner.SW) {
				g.strokeLine(corner.x - 10.0, corner.y + worldExtent.getLength()*zoom - 10.0, corner.x + 10.0, corner.y + worldExtent.getLength()*zoom + 10.0);
				g.strokeLine(corner.x - 10.0, corner.y + worldExtent.getLength()*zoom + 10.0, corner.x + 10.0, corner.y + worldExtent.getLength()*zoom - 10.0);
			}
			else if(mouseOverCorner == ExtentCorner.SE) {
				g.strokeLine(corner.x + worldExtent.getWidth()*zoom - 10.0, corner.y + worldExtent.getLength()*zoom - 10.0, corner.x + worldExtent.getWidth()*zoom + 10.0, corner.y + worldExtent.getLength()*zoom + 10.0);
				g.strokeLine(corner.x + worldExtent.getWidth()*zoom - 10.0, corner.y + worldExtent.getLength()*zoom + 10.0, corner.x + worldExtent.getWidth()*zoom + 10.0, corner.y + worldExtent.getLength()*zoom - 10.0);
			}
		}
	}
	
	
	private void paintGrid(GraphicsContext g) {
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(1.0, zoom*2-0.5))));
		g.setLineWidth(1.0);
		float gridIncrement = 16;
		for(float cx = gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x < getWidth(); cx += gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cx = -gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x > 0; cx -= gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cy = gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y < getHeight(); cy += gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		for(float cy = -gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y > 0; cy -= gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		
		//Draw center cross;
		g.setStroke(Color.web("#000000", 0.5));
		g.setLineWidth(2.0);
		Pixel centerCoordinatePixel = new Pixel(new Coordinate(0, 0), this);
		g.strokeLine(centerCoordinatePixel.x, 0, centerCoordinatePixel.x, getHeight());
		g.strokeLine(0, centerCoordinatePixel.y, getWidth(), centerCoordinatePixel.y);
	}

	private void paintChunk(PreviewChunk chunk, GraphicsContext g) {
		Pixel corner = new Pixel(new Coordinate(chunk.x, chunk.z), this);
		
		if(chunk.hasValidData()) {
			g.drawImage(chunk.raster, corner.x, corner.y);
		}
	}
	
	//Build stuff
	public void updatePreview(boolean clearBuilt) {
		if(clearBuilt) {
			builtChunks.clear();
		}
		
		if(currentBuildTask != null && clearBuilt) {
			currentBuildTask.cancel();
			currentBuildTask = null;
		}
		chunksToBuild.clear();
		
		if(currentPreviewWrapper != null && currentPreviewSynth != null) {
			if(currentPreviewWrapper.wrapperOutputs.size() > 0) {
				ModuleWrapperIO primaryWrapperOutput = currentPreviewWrapper.wrapperOutputs.get(currentPreviewWrapper.module.getOutputs()[0].getName());
				if(primaryWrapperOutput != null) {
					//Veryfy valid datatype
					boolean validDatatype = false;
					AbstractDatatype outputType = primaryWrapperOutput.getDatatype();
					if(outputType instanceof DatatypeHeightmap || outputType instanceof DatatypeColormap || outputType instanceof DatatypeBiomemap || outputType instanceof DatatypeMaterialmap) {
						validDatatype = true;
					}
					if(validDatatype) {
						PreviewChunk[] previewChunks = getPreviewChunksInView(128, currentPreviewSynth, currentPreviewWrapper);
						
						//Filter out chunks that are already built
						for(PreviewChunk c: previewChunks) {
							if(!containsChunk(builtChunks, c)) {
								if(currentBuildTask != null) {
									if(isSameChunk(c, currentBuildTask.getPreviewChunk())) {
										continue;
									}
								}
								chunksToBuild.add(c);
							}
						}
						
						if(chunksToBuild.size() > 0 && currentBuildTask == null) {
							buildChunks();
						}
					}
				}
			}
		}
		
		repaint();
	}
	
	ListChangeListener<WorldExtent> extentListListener = c -> { updatePreview(false); };
	ChangeListener<WorldExtent> currentExtentListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> { updatePreview(false); };
	
	public void updatePreview(Synth synth, ModuleWrapper device) {
		if(currentPreviewSynth != null) {
			currentPreviewSynth.getExtentManager().getObservableExtentsList().removeListener(extentListListener);
			currentPreviewSynth.getExtentManager().currentWorldExtentProperty().removeListener(currentExtentListener);
		}
		
		currentPreviewSynth = synth;
		currentPreviewWrapper = device;
		
		currentPreviewSynth.getExtentManager().getObservableExtentsList().addListener(extentListListener);
		currentPreviewSynth.getExtentManager().currentWorldExtentProperty().addListener(currentExtentListener);
		
		updatePreview(true);
	}
	
	private PreviewChunk[] getPreviewChunksInView(int chunkSize, Synth synth, ModuleWrapper device) {
		long minChunkXCoordinate = (long) Math.floor((getCenterCoordinateX() - getWidth() * 0.5 / getZoom()) / (chunkSize / zoom));
		long maxChunkXCoordinate = (long) Math.floor((getCenterCoordinateX() + getWidth() * 0.5 / getZoom()) / (chunkSize / zoom));
		long minChunkZCoordinate = (long) Math.floor((getCenterCoordinateY() - getHeight() * 0.5 / getZoom()) / (chunkSize / zoom));
		long maxChunkZCoordinate = (long) Math.floor((getCenterCoordinateY() + getHeight() * 0.5 / getZoom()) / (chunkSize / zoom));
		
		ArrayList<PreviewChunk> previewChunks = new ArrayList<PreviewChunk>();
		
		for(long u = minChunkXCoordinate; u <= maxChunkXCoordinate; u++) {
			for(long v = minChunkZCoordinate; v <= maxChunkZCoordinate; v++) {
				previewChunks.add(new PreviewChunk(u, v, chunkSize, getZoom(), synth, device));
			}
		}
		
		//Sort chunks based on distance from preview center
		Collections.sort(previewChunks, (PreviewChunk chunk1, PreviewChunk chunk2) -> {
			double d = chunk1.getCenterDistance(getCenterCoordinateX(), getCenterCoordinateY()) - chunk2.getCenterDistance(getCenterCoordinateX(), getCenterCoordinateY());
			return (int) d;
		});
		
		return previewChunks.toArray(new PreviewChunk[0]);
	}
	
	private void buildChunks() {
		if(chunksToBuild.size() == 0) {
			return;
		}
		
		PreviewChunk chunkToBuild = chunksToBuild.remove(0);
		currentBuildTask = PreviewChunkBuildTask.buildPreviewChunkBuildeTask(chunkToBuild, null);
		
		currentBuildTask.setOnSucceeded(e -> {
			try {
				PreviewChunkBuildTask bt = (PreviewChunkBuildTask) e.getSource();
				AbstractDatatype data = bt.get();
				PreviewChunk builtChunk = bt.getPreviewChunk();
				builtChunk.setData(data);
				builtChunks.add(builtChunk);
				
				currentBuildTask = null;
				if(chunksToBuild.size() > 0) {
					//Continue with next build
					buildChunks();
				}
				
				repaint();
			} catch (InterruptedException | ExecutionException e1) {
				e1.printStackTrace();
			}
		});
		
		new Thread(currentBuildTask).start();
	}
	
	private class PreviewChunk {
		final long chunkX;
		final long chunkZ;
		final int chunkSize;
		final double x;
		final double z;
		final double width;
		final double length;
		final double res;
		final Synth synth;
		final ModuleWrapper wrapper;
		
//		AbstractDatatype chunkData = null;
		WritableImage raster;
		
		public PreviewChunk(long chunkX, long chunkZ, int chunkSize, double zoom, Synth synth, ModuleWrapper device) {
			this.synth = synth;
			this.wrapper = device;
			
			this.chunkX = chunkX;
			this.chunkZ = chunkZ;
			this.chunkSize = chunkSize;
			
			res = 1.0 / zoom;
			x = (double) chunkX * (double) chunkSize * res;
			z = (double) chunkZ * (double) chunkSize * res;
			width = chunkSize * res;
			length = chunkSize * res;
		}
		
		public double getCenterDistance(double x, double z) {
			double chunkCenterX = this.x + width * 0.5;
			double chunkCenterZ = this.z + length * 0.5;
			
			double dx = x - chunkCenterX;
			double dz = z - chunkCenterZ;
			
			return dx * dx + dz * dz;
		}
		
		public void setData(AbstractDatatype data) {
			raster = new WritableImage(chunkSize, chunkSize);
			PixelWriter pw = raster.getPixelWriter();
			
			if(data instanceof DatatypeHeightmap) {
				float[][] hMap = ((DatatypeHeightmap) data).heightMap;
				
				int hMapW = hMap.length;
				int hMapL = hMap[0].length;
				
				for(int u = 0; u < hMapW; u++) {
					for(int v = 0; v < hMapL; v++) {
						pw.setColor(u, v, Color.gray(hMap[u][v]));
					}
				}
			}
			else if(data instanceof DatatypeColormap) {
				float[][][] cMap = ((DatatypeColormap) data).colorMap;
				
				int hMapW = cMap.length;
				int hMapL = cMap[0].length;
				
				for(int u = 0; u < hMapW; u++) {
					for(int v = 0; v < hMapL; v++) {
						pw.setColor(u, v, Color.color(cMap[u][v][0], cMap[u][v][1], cMap[u][v][2]));
					}
				}
			}
			else if(data instanceof DatatypeBiomemap) {
				Biome[][] bMap = ((DatatypeBiomemap) data).getBiomemap();
				
				int hMapW = bMap.length;
				int hMapL = bMap[0].length;
				
				for(int u = 0; u < hMapW; u++) {
					for(int v = 0; v < hMapL; v++) {
						pw.setColor(u, v, bMap[u][v].getFxColor());
					}
				}
			}
			else if(data instanceof DatatypeMaterialmap) {
				MaterialState<?, ?>[][] mMap = ((DatatypeMaterialmap) data).getMaterialmap();
				
				int hMapW = mMap.length;
				int hMapL = mMap[0].length;
				
				for(int u = 0; u < hMapW; u++) {
					for(int v = 0; v < hMapL; v++) {
						pw.setColor(u, v, mMap[u][v].getFxColor());
					}
				}
			}
		}
		
		public boolean hasValidData() {
			return raster != null;
		}
	}
	
	private static class PreviewChunkBuildTask extends BuildTask {
		private final PreviewChunk chunk;
		
		public static PreviewChunkBuildTask buildPreviewChunkBuildeTask(PreviewChunk chunk, BuildStatusListener listener) {
			double x = chunk.x;
			double y = 0.0;
			double z = chunk.z;
			
			double width = chunk.width;
			double height = 0.0;
			double length = chunk.length;
			
			double resolutionPitch = chunk.res;
			
			ModuleWrapperIO primaryWrapperOutput = chunk.wrapper.wrapperOutputs.get(chunk.wrapper.module.getOutputs()[0].getName());
			AbstractDatatype requestData = primaryWrapperOutput.getDatatype().getPreviewDatatype(x, y, z, width, height, length, resolutionPitch);
			ModuleOutput output = (ModuleOutput) primaryWrapperOutput.getIO();
			ModuleOutputRequest request = new ModuleOutputRequest(output, requestData);
			
			return new PreviewChunkBuildTask(chunk.synth, chunk.wrapper, request, chunk);
		}
		
		private PreviewChunkBuildTask(Synth synth, ModuleWrapper device, ModuleOutputRequest request, PreviewChunk chunk) {
			super(synth, device, request, null);
			this.chunk = chunk;
		}
		
		public PreviewChunk getPreviewChunk() {
			return chunk;
		}
		
	}
	
	private boolean containsChunk(ArrayList<PreviewChunk> chunkList, PreviewChunk chunk) {
		for(PreviewChunk c: chunkList) {
			if(isSameChunk(chunk, c)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isSameChunk(PreviewChunk chunk1, PreviewChunk chunk2) {
		if(chunk1.chunkX == chunk2.chunkX && chunk1.chunkZ == chunk2.chunkZ && chunk1.wrapper == chunk2.wrapper && chunk1.res == chunk2.res) {
			return true;
		}
		return false;
	}
	
	private boolean chunkInView(PreviewChunk chunk) {
		int chunkSize = chunk.chunkSize;
		long minChunkXCoordinate = (long) Math.floor((getCenterCoordinateX() - getWidth() * 0.5 / getZoom()) / (chunkSize / zoom));
		long maxChunkXCoordinate = (long) Math.floor((getCenterCoordinateX() + getWidth() * 0.5 / getZoom()) / (chunkSize / zoom));
		long minChunkZCoordinate = (long) Math.floor((getCenterCoordinateY() - getHeight() * 0.5 / getZoom()) / (chunkSize / zoom));
		long maxChunkZCoordinate = (long) Math.floor((getCenterCoordinateY() + getHeight() * 0.5 / getZoom()) / (chunkSize / zoom));
		
		long x = chunk.chunkX;
		long z = chunk.chunkZ;
		
		if(x >= minChunkXCoordinate && x <= maxChunkXCoordinate && z >= minChunkZCoordinate && z <= maxChunkZCoordinate) {
			return true;
		}
		return false;
	}
	
	private void hoverExtentFromCoordinate(double x, double z) {
		Pixel mousePixel = new Pixel(x, z);
		Coordinate mouseCoordinate = new Coordinate(mousePixel, this);
		WorldExtent topExtent = null;
		double closestCornerDiststance = Double.MAX_VALUE;
		
		for(WorldExtent extent: currentPreviewSynth.getExtentManager().getObservableExtentsList()) {
			double cornerDist = extent.distanceToClosestConer(mouseCoordinate.x, mouseCoordinate.y) * zoom;
			if((extent.containsCoordinate(mouseCoordinate.x, mouseCoordinate.y) && closestCornerDiststance > 20.0) || (cornerDist <= 20.0 && cornerDist < closestCornerDiststance)) {
				if(topExtent == null) {
					topExtent = extent;
				}
				else if(extent.getArea() < topExtent.getArea() || cornerDist < closestCornerDiststance) {
					topExtent = extent;
				}
				
				if(cornerDist < closestCornerDiststance) {
					closestCornerDiststance = cornerDist;
				}
			}
		}
		mouseOverExtent = topExtent;
		mouseOverCorner = null;
		if(closestCornerDiststance <= 20.0) {
			mouseOverCorner = topExtent.closestCorner(mouseCoordinate.x, mouseCoordinate.y);
		}
	}
}
