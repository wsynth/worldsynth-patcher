package net.worldsynth.patcher.resources;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class Resources {
	
	public static final URL getResourceURL(String resource) {
		ClassLoader clasLoader = Resources.class.getClassLoader();
		URL resourceURL = clasLoader.getResource(resource);
		
		if(resourceURL == null) {
			throw new IllegalArgumentException("Resource: \"" + resource + "\" not found");
		}
		return resourceURL;
	}
	
	public static final File getResourceFile(String resource) {
		URL resourceURL = getResourceURL(resource);
		
		if(resourceURL == null) {
			throw new IllegalArgumentException("Resource: \"" + resource + "\" not found");
		}
		return new File(resourceURL.getFile());
	}
	
	public static final InputStream getResourceStream(String resource) {
		return Resources.class.getClassLoader().getResourceAsStream(resource);
	}
}
