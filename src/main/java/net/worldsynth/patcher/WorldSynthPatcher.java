/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher;

import java.awt.Window.Type;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.worldsynth.addon.IAddonLoader;
import net.worldsynth.common.Commons;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.common.WorldSynthDirectoryConfig;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.glpreview.util.GLUtil;
import net.worldsynth.patcher.resources.Resources;
import net.worldsynth.patcher.statistics.WorldSynthStatTracker;
import net.worldsynth.patcher.ui.fx.WorldSynthEditorController;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorPane;
import net.worldsynth.standalone.ui.stage.StageManager;

public class WorldSynthPatcher extends Application {
	
	public static final String VERSION = "N/A";
	
	public static final Logger LOGGER = LogManager.getLogger("worldsynth-patcher");
	
	public static String stylesheet;
	public static WorldSynthPatcher instance;
	public static Stage primaryStage;
	
	private static IAddonLoader[] injectedModuleRegisters;
	
	//Arguments
	private static boolean openSplash = !Commons.systemIsMac();
	
	public static void main(String[] args) {
		parseArguments(args);
		injectedModuleRegisters = null;
		launch(args);
	}
	
	/**
	 * Start Patcher with module registers passed as arguments in addition to addons loaded from addon jars.
	 * This is practical for loading modules in addons under development.
	 * 
	 * @param args as in main args
	 * @param injectedAddonLoaders instances of module registers to be loaded
	 */
	public static void startPatcher(String[] args, IAddonLoader... injectedAddonLoaders) {
		parseArguments(args);
		injectedModuleRegisters = injectedAddonLoaders;
		launch(args);
	}
	
	private static File biomesDirectory;
	private static File materialsDirectory;
	private static File addonsDirectory;
//	private static File dataDirectory;
	private static void parseArguments(String[] args) {
		for(int i = 0; i < args.length; i++) {
			String arg = args[i];
			if(arg.equals("--help") || arg.equals("-h")) {
				System.out.println("   --help             Print a list of available arguments");
				System.out.println("   -h                 Print a list of available arguments");
				System.out.println("   --nosplash         Start without showing splash");
				System.out.println("   biomes <path>      Path to biomes directory");
				System.out.println("   materials <path>   Path to materials directory");
				System.out.println("   addons <path>       Path to addon directory");
//				System.out.println("   data <path>        Path to data directory");
				System.exit(0);
			}
			else if(arg.equals("--nosplash")) {
				openSplash = false;
			}
			else if(arg.equals("biomes")) {
				biomesDirectory = new File(args[++i]);
			}
			else if(arg.equals("materials")) {
				materialsDirectory = new File(args[++i]);
			}
			else if(arg.equals("addons")) {
				addonsDirectory = new File(args[++i]);
			}
//			else if(arg.equals("data")) {
//				dataDirectory = new File(args[++i]);
//			}
			else {
				System.out.println("Invalid argument: " + arg);
				System.exit(1);
			}
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		instance = this;
		
		LOGGER.info("JRE: " + System.getProperty("java.version"));
		LOGGER.info("OS: " + System.getProperty("os.name"));
		GLUtil.logGLProfiles();
		LOGGER.info("Starting WorldSynth standalone editor");
		
		JFrame splash = new JFrame();
		if(openSplash) {
			LOGGER.info("Displaying patcher splash screen");
			splash.setLocationRelativeTo(null);
			splash.setUndecorated(true);
			splash.setType(Type.UTILITY);
			splash.add(new JLabel(new ImageIcon(Resources.getResourceURL("Splash.jpg"))));
			splash.pack();
			splash.setLocation(splash.getLocation().x-splash.getWidth()/2, splash.getLocation().y-splash.getHeight()/2);
			splash.setVisible(true);
		}
		
		LOGGER.info("Initializing engine");
		LOGGER.info("Execution directory: " + Commons.getExecutionDirectory());
		WorldSynthDirectoryConfig directoryConfig = new WorldSynthDirectoryConfig(Commons.getExecutionDirectory(), addonsDirectory, materialsDirectory, biomesDirectory);
		new WorldSynthCore(directoryConfig, injectedModuleRegisters);
		
		LOGGER.info("Initializing extent manager");
		new WorldExtentManager();
		LOGGER.info("Initializing statistics tracker");
		new WorldSynthStatTracker();
		LOGGER.info("Initializing stage manager");
		new StageManager();
		
		/////////////////////////////////////////////////////////////////////////////////
		
		LOGGER.info("Setting up primary stage");
		WorldSynthPatcher.primaryStage = primaryStage;
		primaryStage.setTitle("WorldSynth Patcher - Version " + VERSION);
		Image stageIcon = new Image(Resources.getResourceStream("worldSynthIcon.png"));
		primaryStage.getIcons().add(stageIcon);
		
		LOGGER.info("Registering primary stage with the stage manager");
		StageManager.setPrimaryStage(primaryStage);
		
		LOGGER.info("Setting up root scene");
		Parent root = FXMLLoader.load(Resources.getResourceURL("WorldSynthMainScene.fxml"));
		Scene scene = new Scene(root, 1500, 800);
		scene.addEventHandler(KeyEvent.ANY, e -> {
			Node tabContentNode =  WorldSynthEditorController.instance.editorTabPane.getSelectionModel().getSelectedItem().getContent();
			if(tabContentNode instanceof SynthEditorPane) {
				SynthEditorPane currentEditor = (SynthEditorPane) tabContentNode;
				currentEditor.getKeyboardListener().handle(e);
			}
		});
		
		LOGGER.info("Applying dark theme");
		stylesheet = Resources.getResourceURL("DarkThemeBase.css").toExternalForm();
		scene.getStylesheets().add(stylesheet);
		
		LOGGER.info("Set stage scene and size, and show it");
		primaryStage.setScene(scene);
		primaryStage.setMinWidth(800);
		primaryStage.setMinHeight(700);
		primaryStage.show();
		
		if(openSplash) {
			LOGGER.info("Disposing patcher splash screen");
			splash.dispose();
		}
	}
}
