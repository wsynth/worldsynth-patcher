/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import net.worldsynth.common.Commons;
import net.worldsynth.module.AbstractModule;

public class WorldSynthStatTracker {
	
	private static File moduleStatFile;
	
	private static HashMap<String, Integer> moduleStat = new HashMap<String, Integer>();
	
	public WorldSynthStatTracker() {
		File execDir = Commons.getExecutionDirectory();
		File dataDirectory = new File(execDir, "data");
		if(!dataDirectory.exists()) {
			dataDirectory.mkdir();
		}
		
		moduleStatFile = new File(dataDirectory, "modulestat.csv");
		
		try {
			readModuleStat(moduleStatFile);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static void incrementModuleUse(Class<? extends AbstractModule> module) {
		String key = module.getName();
		
		Object value = moduleStat.get(key);
		int count = 0;
		
		if(value != null) {
			count = (Integer) value;
		}
		
		count++;
		moduleStat.put(key, count);
		
		try {
			writeMouduleStatFile(moduleStatFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static int getModuleUseCount(String moduleClass) {
		Object value = moduleStat.get(moduleClass);
		int count = 0;
		
		if(value != null) {
			count = (Integer) value;
		}
		
		return count;
	}
	
	private static void readModuleStat(File moduleStatFile) throws IOException {
		if(!moduleStatFile.exists()) {
			return;
		}
		moduleStat.clear();
		
		BufferedReader reader = new BufferedReader(new FileReader(moduleStatFile));
		String line = null;
		while((line = reader.readLine()) != null) {
			line.replace("\t", "");
			String[] entries = line.split(";");
			
			if(entries.length < 2) {
				continue;
			}
			
			int count;
			try {
				count = Integer.parseInt(entries[1]);
			} catch (Exception e) {
				continue;
			}
			
			moduleStat.put(entries[0], count);
		}
		
		reader.close();
	}
	
	private static void writeMouduleStatFile(File moduleStatFile) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(moduleStatFile));
		
		Iterator<Entry<String, Integer>> it = moduleStat.entrySet().iterator();
		while(it.hasNext()) {
			Entry<String, Integer> par = (Entry<String, Integer>) it.next();
			String key = par.getKey();
			int value = par.getValue();
			writer.write(key + ";" + String.valueOf(value));
			writer.newLine();
		}
		
		writer.close();
	}
}
