package net.worldsynth.glpreview.util;

import org.apache.logging.log4j.Logger;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLCapabilitiesImmutable;
import com.jogamp.opengl.GLProfile;

import net.worldsynth.patcher.WorldSynthPatcher;

public class GLUtil {
	
	private static Logger LOGGER = WorldSynthPatcher.LOGGER;
	
	public static void logGLProfiles() {
		GLProfile glp = GLProfile.getDefault();
		final GLCapabilitiesImmutable glcaps = (GLCapabilitiesImmutable) new GLCapabilities(glp);
		final GLCapabilities tGLCapabilities = new GLCapabilities(glp);

		LOGGER.info("Default GL Profile Details: " + glp.toString());
		LOGGER.info("System Capabilities:" + glcaps.toString());
	}
}
