/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.fx;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;
import net.worldsynth.glpreview.heightmap.HeightmapOverlay3DGLPreviewBufferedGL;

public class Overlay3dViewer extends BorderPane {
	
	private final HeightmapOverlay3DGLPreviewBufferedGL openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Overlay3dViewer() {
		openGL3DRenderer = new HeightmapOverlay3DGLPreviewBufferedGL();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setHeightmap(float[][] heightmap, float[][][] colormap, float maxHeightUnit, double size) {
		openGL3DRenderer.setHeightmap(heightmap, colormap, maxHeightUnit, size);
	}
}
