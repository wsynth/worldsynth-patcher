/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.fx;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;
import net.worldsynth.glpreview.featurespace.Featurespace3DGLPreviewBufferedGL;

public class Featurespace3dViewer extends BorderPane {
	
	private final Featurespace3DGLPreviewBufferedGL openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Featurespace3dViewer() {
		openGL3DRenderer = new Featurespace3DGLPreviewBufferedGL();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setFeaturespace(double[][] points, double x, double y, double z, double width, double height, double length) {
		openGL3DRenderer.setFeaturespace(points, x, y, z, width, height, length);
	}
}
