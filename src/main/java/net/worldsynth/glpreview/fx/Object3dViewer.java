/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.fx;

import javafx.embed.swing.SwingNode;
import javafx.scene.layout.BorderPane;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.glpreview.voxel.Object3DGLPreviewBufferedGL;

public class Object3dViewer extends BorderPane {
	
	private final Object3DGLPreviewBufferedGL openGL3DRenderer;
	private final SwingNode swingNode = new SwingNode();
	
	public Object3dViewer() {
		openGL3DRenderer = new Object3DGLPreviewBufferedGL();
		swingNode.setContent(openGL3DRenderer);
		setCenter(swingNode);
	}
	
	public void setObject(CustomObject object) {
		openGL3DRenderer.setObject(object.blocks, object.width, object.height, object.length, object.xMin, object.yMin, object.zMin, object.xMax, object.yMax, object.zMax);
	}
}
