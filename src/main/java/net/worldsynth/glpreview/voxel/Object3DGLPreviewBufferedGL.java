/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.voxel;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

import net.worldsynth.customobject.Block;
import net.worldsynth.glpreview.Line;
import net.worldsynth.glpreview.buffered.BufferedGLPanel;
import net.worldsynth.glpreview.model.AbstractLineModel;
import net.worldsynth.material.MaterialState;

public class Object3DGLPreviewBufferedGL extends BufferedGLPanel {
	private static final long serialVersionUID = -1895542307473079689L;
	
	private VoxelModel voxelModel;
	
	public Object3DGLPreviewBufferedGL() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL2);
		GLCapabilities glcapabilities = new GLCapabilities(glprofile);
		setRequestedGLCapabilities(glcapabilities);
	}
	
	public void setObject(Block[] object, int width, int height, int length, int xMin, int yMin, int zMin, int xMax, int yMax, int zMax) {
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[width][height][length];
		for(Block block: object) {
			blockspace[block.x - xMin][block.y - yMin][block.z - zMin] = block.material;
		}
		
//		voxelModel = new VoxelModel(blockspaceMaterialId);
		float xOffset = (float) Math.ceil(width / 2.0) - (float) xMax - 0.5f;
		float yOffset = (float) Math.ceil(height / 2.0) - (float) yMax - 0.5f;
		float zOffset = (float) Math.ceil(length / 2.0) - (float) zMax - 0.5f;
		voxelModel = new VoxelModel(blockspace, xOffset, yOffset, zOffset);
		
		startNewModel();
		if(voxelModel.getPrimitivesCount() > 0) {
			loadModel(voxelModel);
		}
		loadModel(new CrosshairModel());
		endNewModel();
		display();
	}
	
	private class CrosshairModel extends AbstractLineModel {
		
		public CrosshairModel() {
			initVertexArray(3);
			
			Line line = new Line();
			line.setVertex(0, -100, 0, 0);
			line.setVertex(1, 100, 0, 0);
			line.setColor(1.0f, 0.0f, 0.0f);
			insertVertexArray(line, 0);
			
			line.setVertex(0, 0, -100, 0);
			line.setVertex(1, 0, 100, 0);
			line.setColor(0.0f, 1.0f, 0.0f);
			insertVertexArray(line, 1);
			
			line.setVertex(0, 0, 0, -100);
			line.setVertex(1, 0, 0, 100);
			line.setColor(0.0f, 0.0f, 1.0f);
			insertVertexArray(line, 2);
		}

		@Override
		public int getPrimitivesCount() {
			return 3;
		}
	}
}
