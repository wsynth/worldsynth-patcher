/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.glpreview.voxel;

import net.worldsynth.common.color.WsColor;
import net.worldsynth.glpreview.QuadFace;
import net.worldsynth.glpreview.model.AbstractQuadfaceModel;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class VoxelModel extends AbstractQuadfaceModel {
	
	public MaterialState<?, ?>[][][] blockspace;
	
	private int facesCount = 0;
	
	private int modelWidth;
	private int modelHeight;
	private int modelLength;
	
	private float offsetX = 0;
	private float offsetY = 0;
	private float offsetZ = 0;
	
	public VoxelModel(MaterialState<?, ?>[][][] blockspace) {
		this(blockspace, 0, 0, 0);
	}
	
	public VoxelModel(MaterialState<?, ?>[][][] blockspace, float offsetX, float offsetY, float offsetZ) {
		this.blockspace = blockspace;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.offsetZ = offsetZ;
		
		modelWidth = blockspace.length;
		if(modelWidth == 0) {
			return;
		}
		modelHeight = blockspace[0].length;
		if(modelHeight == 0) {
			return;
		}
		modelLength = blockspace[0][0].length;
		if(modelLength == 0) {
			return;
		}
		
		generateFaces(getBooleanFacesArray(blockspace));
	}
	
	@Override
	public int getPrimitivesCount() {
		return facesCount;
	}
	
	private boolean[][][][] getBooleanFacesArray(MaterialState<?, ?>[][][] blockspace) {
		/* Faces
		 * 0 = FRONT
		 * 1 = BACK
		 * 2 = BOTOM
		 * 3 = TOP
		 * 4 = RIGHT
		 * 5 = LEFT
		 */
		
		int faceCount = 0;
		
		int zx = blockspace.length;
		int zy = blockspace[0].length;
		int zz = blockspace[0][0].length;
		
		boolean[][][][] voxelFace = new boolean[zx][zy][zz][6];
		
		for(int x = 0; x < zx; x++) {
			for(int y = 0; y < zy; y++) {
				for(int z = 0; z < zz; z++) {
					//Face FRONT
					if(z == zz-1) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][0] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x][y][z+1]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][0] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][0] = false;
					}
					
					//Face BACK
					if(z == 0) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][1] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x][y][z-1]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][1] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][1] = false;
					}
					
					//Face BOTOM
					if(y == 0) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][2] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x][y-1][z]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][2] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][2] = false;
					}
					
					//Face TOP
					if(y == zy-1) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][3] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x][y+1][z]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][3] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][3] = false;
					}
					
					//Face RIGHT
					if(x == zx-1) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][4] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x+1][y][z]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][4] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][4] = false;
					}
					
					//Face LEFT
					if(x == 0) {
						if(!MaterialRegistry.isAir(blockspace[x][y][z])) {
							voxelFace[x][y][z][5] = true;
							faceCount++;
						}
					}
					else if(MaterialRegistry.isAir(blockspace[x-1][y][z]) && !MaterialRegistry.isAir(blockspace[x][y][z])) {
						voxelFace[x][y][z][5] = true;
						faceCount++;
					}
					else {
						voxelFace[x][y][z][5] = false;
					}
				}
			}
		}
		
		this.facesCount = faceCount;
		
		return voxelFace;
	}
	
	private void generateFaces(boolean[][][][] voxelFaces) {
		int zx = voxelFaces.length;
		int zy = voxelFaces[0].length;
		int zz = voxelFaces[0][0].length;
		
		initVertexArray(facesCount);
		
		/* Faces
		 * 0 = FRONT
		 * 1 = BACK
		 * 2 = BOTOM
		 * 3 = TOP
		 * 4 = RIGHT
		 * 5 = LEFT
		 */
		
		int index = 0;
		QuadFace tempFace = new QuadFace();
		
		for(int x = 0; x < zx; x++) {
			for(int y = 0; y < zy; y++) {
				for(int z = 0; z < zz; z++) {
					//Face FRONT
					if(voxelFaces[x][y][z][0]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(0.0f, 0.0f, 1.0f);
						
						tempFace.setVertex(0, x, y, z+1);
						tempFace.setVertex(1, x+1, y, z+1);
						tempFace.setVertex(2, x+1, y+1, z+1);
						tempFace.setVertex(3, x, y+1, z+1);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
					//Face BACK
					if(voxelFaces[x][y][z][1]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(0.0f, 0.0f, -1.0f);
						
						tempFace.setVertex(0, x, y+1, z);
						tempFace.setVertex(1, x+1, y+1, z);
						tempFace.setVertex(2, x+1, y, z);
						tempFace.setVertex(3, x, y, z);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
					
					//Face BOTOM
					if(voxelFaces[x][y][z][2]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(0.0f, -1.0f, 0.0f);
						
						tempFace.setVertex(0, x, y, z);
						tempFace.setVertex(1, x+1, y, z);
						tempFace.setVertex(2, x+1, y, z+1);
						tempFace.setVertex(3, x, y, z+1);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
					
					//Face TOP
					if(voxelFaces[x][y][z][3]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(0.0f, 1.0f, 0.0f);
						
						tempFace.setVertex(0, x, y+1, z+1);
						tempFace.setVertex(1, x+1, y+1, z+1);
						tempFace.setVertex(2, x+1, y+1, z);
						tempFace.setVertex(3, x, y+1, z);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
					
					//Face RIGHT
					if(voxelFaces[x][y][z][4]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(1.0f, 0.0f, 0.0f);
						
						tempFace.setVertex(0, x+1, y+1, z+1);
						tempFace.setVertex(1, x+1, y, z+1);
						tempFace.setVertex(2, x+1, y, z);
						tempFace.setVertex(3, x+1, y+1, z);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
					
					//Face LEFT
					if(voxelFaces[x][y][z][5]) {
						float[] color = getColor(blockspace[x][y][z]);
						tempFace.setColor(color[0], color[1], color[2]);
						tempFace.setNormal(-1.0f, 0.0f, 0.0f);
						
						tempFace.setVertex(0, x, y, z+1);
						tempFace.setVertex(1, x, y+1, z+1);
						tempFace.setVertex(2, x, y+1, z);
						tempFace.setVertex(3, x, y, z);
						
						tempFace.translate(-modelWidth / 2 - offsetX, -modelHeight / 2 - offsetY, -modelLength / 2 - offsetZ);
						
						insertVertexArray(tempFace, index++);
					}
				}
			}
		}
	}
	
	private float[] getColor(MaterialState<?, ?> material) {
		WsColor c = material.getWsColor();
		float red = c.getRed();
		float green = c.getGreen();
		float blue = c.getBlue();
		return new float[] {red, green, blue};
	}
}
