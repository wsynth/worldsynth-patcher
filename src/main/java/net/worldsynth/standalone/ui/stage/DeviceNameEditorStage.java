/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorPane;

public class DeviceNameEditorStage extends PinnableUtilityStage {
	TextField customNameField;
	
	public DeviceNameEditorStage(ModuleWrapper device, SynthEditorPane synthEditor) {
		setTitle(device.module.getModuleName() + " custom name editor");
		
		BorderPane rootPane = new BorderPane();
		
		customNameField = new TextField(device.getCustomName());
		customNameField.setPrefColumnCount(20);
		
		//Pin button
		Image pinIcon = new Image(getClass().getResourceAsStream("pin16x16_white.png"));
		ToggleButton pinButton = new ToggleButton(null, new ImageView(pinIcon));
		pinButton.setOnAction(e -> {
			setPinned(pinButton.isSelected());
		});
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> {
			close();
		});
		
		Button applyButton = new Button("Apply");
		applyButton.setOnAction(e -> {
			synthEditor.renameDevice(device, customNameField.getText());
		});
		
		Button okButton = new Button("OK");
		okButton.setOnAction(e -> {
			synthEditor.renameDevice(device, customNameField.getText());
			close();
		});
		
		HBox buttonsPane = new HBox();
		buttonsPane.getChildren().add(pinButton);
		
		Pane spacerPane = new Pane();
		HBox.setHgrow(spacerPane, Priority.SOMETIMES);
		buttonsPane.getChildren().add(spacerPane);
		
		buttonsPane.getChildren().add(cancelButton);
		buttonsPane.getChildren().add(applyButton);
		buttonsPane.getChildren().add(okButton);
		
		GridPane namingPane = new GridPane();
		namingPane.add(new Label("Custom name"), 0, 0);
		namingPane.add(customNameField, 1, 0);
		namingPane.setPadding(new Insets(10.0));
		namingPane.setHgap(10.0);
		
		//Setup keybinds
		namingPane.setOnKeyReleased(e -> {
			if(e.getCode() == KeyCode.ENTER) {
				synthEditor.renameDevice(device, customNameField.getText());
			}
		});
		
		rootPane.setCenter(namingPane);
		rootPane.setBottom(buttonsPane);
		
		Scene scene = new Scene(rootPane, rootPane.getPrefWidth(), rootPane.getPrefHeight());
		scene.getStylesheets().add(WorldSynthPatcher.stylesheet);
		setScene(scene);
		sizeToScene();
		show();
	}
}
