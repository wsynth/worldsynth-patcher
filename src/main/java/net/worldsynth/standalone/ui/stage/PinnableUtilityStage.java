/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

public class PinnableUtilityStage extends UtilityStage {
	
	private boolean pinned = false;
	
	public PinnableUtilityStage() {}
	
	public void setPinned(boolean pin) {
		pinned = pin;
	}
	
	public boolean isPinned() {
		return pinned;
	}
}
